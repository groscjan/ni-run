#!/bin/bash

sudo add-apt-repository ppa:ubuntu-toolchain-r/ppa
sudo apt-get update
sudo apt-get install cmake g++-10 curl
if [ $? -ne 0 ]; then
    exit 1
fi

sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-10 99

if ! command -v cargo &> /dev/null
then
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    source "$HOME"/.cargo/env
fi
git clone https://github.com/kondziu/FML.git

cd FML
cargo build --release
cd ..

./hw/02/build.sh
./hw/03/build.sh
./hw/04/build.sh

