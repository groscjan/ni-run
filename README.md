# NI-RUN


## How to run

First run the `build.sh` script, which gathers all dependencies and builds them.

Now you can run one of the shell scripts:
* `./fml_ast_interpreter.sh run <program.fml>` to interpret a fml file using the ast interpreter.   
* `./fml_bc_interpreter.sh <program.bc>` to interpret a bytecode file using the bc interpreter.
* `./fml_bc_compiler.sh <program.fml>` to compile a fml file into bytecode and print it to stdout.
* `./fml.sh run <program.fml>` to combine the compilation and bytecode interpretation steps.

