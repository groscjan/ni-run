

#include "ast/node.hpp"
#include "ast/nodes/block.hpp"
#include "ast/nodes/var.hpp"
#include "ast/nodes/integer.hpp"
#include "ast/nodes/print.hpp"
#include "ast/nodes/var_access.hpp"

#include "ast/visitors/impl/interpreter.hpp"

#include <iostream>
#include <parser/json_ast_parser.hpp>

void print_usage ( int argc, char * argv [] )
{
    std::cout << "Usage: " << argv[0] << " <ast_json_filepath>" << std::endl;
}

int main ( int argc, char * argv [] )
{
    if (argc != 2)
    {
        print_usage(argc, argv);
        return 1;
    }

    auto parser = json_ast_parser {};
    auto top = parser.parse_ast(argv[1]);

    auto interpret = std::make_unique<interpreter>();
    //try
    {
        top . accept ( interpret . get () );
    }
   // catch ( const std::runtime_error & e )
    {
   //     std::cerr << "Runtime error occurred: " << e . what() << '\n';
    }
}
