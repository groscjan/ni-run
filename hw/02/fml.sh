#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 run <program.fml>"
    exit 1
fi

base_dir=`dirname "$0"`

fml_exe="$base_dir"/../../FML/target/release/fml
interpreter_exe="$base_dir"/build/ast
temp_json=tmp.json

./"$fml_exe" parse --format JSON < "$2" > "$temp_json"

if [ "$?" -ne 0 ]; then
    exit 1
fi 

./"$interpreter_exe" "$temp_json"
rm "$temp_json"



