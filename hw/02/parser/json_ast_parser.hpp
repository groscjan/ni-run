//
// Created by Jan Groschaft on 3/4/21.
//

#ifndef INC_02_JSON_AST_PARSER_HPP
#define INC_02_JSON_AST_PARSER_HPP

#include "ast/node.hpp"

class json_ast_parser
{
public:
    ast::node parse_ast ( const std::string & file_path );
};


#endif //INC_02_JSON_AST_PARSER_HPP
