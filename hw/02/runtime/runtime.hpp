//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_RUNTIME_HPP
#define INC_02_RUNTIME_HPP

#include "stack_frame.hpp"
#include "heap.hpp"
#include "ast/nodes/obj_ref.hpp"
#include "ast/nodes/object.hpp"

#include <list>

class runtime
{
    std::list<stack_frame> _frames {};
    stack_frame * _base_frame { nullptr }; //base frames are visible in functions
    heap _heap {};
public:
    stack_frame & push_frame(bool connect_to_base = false);

    stack_frame & push_base_frame();

    stack_frame pop_frame();

    [[nodiscard]] stack_frame & get_current_frame();

    struct instance;

    [[nodiscard]] ast::nodes::obj_ref alloc ( ast::nodes::object obj );

    [[nodiscard]] ast::nodes::object& get_object ( const ast::nodes::obj_ref & ref );

    [[nodiscard]] const ast::nodes::object& get_object ( const ast::nodes::obj_ref & ref ) const;

    [[nodiscard]] ast::nodes::arr_ref alloc_array ( ast::nodes::array arr );

    [[nodiscard]] ast::nodes::array& get_array ( const ast::nodes::arr_ref & ref );

    [[nodiscard]] const ast::nodes::array& get_array ( const ast::nodes::arr_ref & ref ) const;
};


#endif //INC_02_RUNTIME_HPP
