//
// Created by Jan Groschaft on 3/3/21.
//

#include "runtime.hpp"

#include "ast/nodes/obj_def.hpp"


stack_frame & runtime::push_frame ( bool connect_to_base )
{
    auto * prev_frame = connect_to_base ? _base_frame : _frames.empty() ? nullptr : &_frames.back();
    auto new_frame = stack_frame { prev_frame };
    _frames.push_back( std::move ( new_frame ) );
    return _frames.back();
}

stack_frame & runtime::push_base_frame ( )
{
    auto * prev_frame = _base_frame;
    auto new_frame = stack_frame { prev_frame };
    _frames.push_back( std::move ( new_frame ) );
    _base_frame = &_frames.back();
    return _frames.back();
}

stack_frame runtime::pop_frame ( )
{
    if ( &_frames.back() == _base_frame )
        _base_frame = _frames.back().get_parent();
    auto res = std::move ( _frames . back() );
    res . set_parent( nullptr );
    _frames.pop_back();
    return res;
}

stack_frame & runtime::get_current_frame ( )
{
    return _frames.back();
}

ast::nodes::obj_ref runtime::alloc ( ast::nodes::object obj )
{
    return _heap.alloc( std::move ( obj ) );
}

ast::nodes::object& runtime::get_object ( const ast::nodes::obj_ref & ref )
{
    return _heap . get ( ref );
}

const ast::nodes::object & runtime::get_object ( const ast::nodes::obj_ref & ref ) const
{
    return _heap . get ( ref );
}

ast::nodes::arr_ref runtime::alloc_array ( ast::nodes::array arr )
{
    return _heap.alloc_array( std::move(arr) );
}

ast::nodes::array & runtime::get_array ( const ast::nodes::arr_ref & ref )
{
    return _heap.get_array( ref );
}

const ast::nodes::array & runtime::get_array ( const ast::nodes::arr_ref & ref ) const
{
    return _heap.get_array( ref );
}





