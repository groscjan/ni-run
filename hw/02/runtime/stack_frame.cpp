//
// Created by Jan Groschaft on 3/3/21.
//

#include "stack_frame.hpp"
#include "ast/nodes/null.hpp"

#include <stdexcept>

stack_frame::stack_frame ( stack_frame * parent ) : _parent ( parent )
{
}

ast::node stack_frame::get_variable ( const std::string & name ) const
{
    if ( auto it = _variables.find(name); it != std::end(_variables) )
        return it -> second;
    return _parent ? _parent -> get_variable( name ) :
           throw std::runtime_error ( "Lookup failed: "s + name + " doesn't exist in the current scope.");
}


ast::node stack_frame::get_function ( const std::string & name ) const
{
    if ( auto it = _functions.find(name); it != std::end(_functions) )
        return it -> second;
    return _parent ? _parent -> get_function( name ) :
           throw std::runtime_error ( "Lookup failed: "s + name + " doesn't exist in the current scope.");
}




void stack_frame::create_variable ( std::string name, ast::node val )
{
    if ( auto it = _variables.find(name); it != std::end(_variables) )
        throw std::runtime_error ( std::string ("Redefinition of " + name ));
    _variables.emplace(std::move(name), std::move(val) );
}

void stack_frame::create_function ( std::string name, ast::node val )
{
    if ( auto it = _functions.find(name); it != std::end(_functions) )
        throw std::runtime_error ( std::string ("Redefinition of " + name ));
    _functions.emplace(std::move(name), std::move(val) );
}

void stack_frame::assign_variable ( const std::string & name, ast::node val )
{
    if ( auto it = _variables.find(name); it != std::end(_variables) )
    {
        it -> second = std::move ( val );
        return;
    }
    _parent ? _parent -> assign_variable( name, std::move(val) ) :
    throw std::runtime_error ( std::string ("Assignment failed: variable ") + name + " doesn't exist in current scope.");
}

stack_frame * stack_frame::get_parent ( ) const
{
    return _parent;
}

void stack_frame::set_parent ( stack_frame * parent )
{
    _parent = parent;
}

const std::map<std::string, ast::node> & stack_frame::get_variables ( ) const
{
    return _variables;
}

bool stack_frame::has_function ( const std::string & name ) const
{
    return _functions . find( name ) != std::end ( _functions );
}





