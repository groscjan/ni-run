//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_STACK_FRAME_HPP
#define INC_02_STACK_FRAME_HPP

#include "ast/node.hpp"

#include <map>
#include <string>

class stack_frame
{
    std::map<std::string, ast::node> _variables;
    std::map<std::string, ast::node> _functions;
    stack_frame * _parent;

public:

    explicit stack_frame (stack_frame * parent = nullptr);

    [[nodiscard]] ast::node get_variable ( const std::string & name ) const;

    [[nodiscard]] const std::map<std::string, ast::node> & get_variables ( ) const;

    [[nodiscard]] ast::node get_function ( const std::string & name ) const;

    [[nodiscard]] bool has_function ( const std::string & name ) const;

    void create_variable ( std::string name, ast::node val );

    void create_function ( std::string name, ast::node val );

    void assign_variable ( const std::string & name, ast::node val );

    [[nodiscard]] stack_frame * get_parent ( ) const;

    void set_parent ( stack_frame * parent );
};


#endif //INC_02_STACK_FRAME_HPP
