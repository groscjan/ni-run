//
// Created by Jan Groschaft on 3/7/21.
//

#include "heap.hpp"

#include <cassert>

ast::nodes::obj_ref heap::alloc ( ast::nodes::object o )
{
    _object_pool . push_back( std::move ( o ) );
    return ast::nodes::obj_ref { _object_pool.size() - 1 };
}

ast::nodes::object & heap::get ( const ast::nodes::obj_ref & ref )
{
    assert( ref . heap_idx < _object_pool . size() && "Heap error: invalid object reference.");
    return _object_pool [ ref.heap_idx ];
}

const ast::nodes::object & heap::get ( const ast::nodes::obj_ref & ref ) const
{
    assert( ref . heap_idx < _object_pool . size() && "Heap error: invalid object reference.");
    return _object_pool [ ref.heap_idx ];
}

ast::nodes::arr_ref heap::alloc_array ( ast::nodes::array arr )
{
    _array_pool . push_back( std::move ( arr ) );
    return ast::nodes::arr_ref { _array_pool.size() - 1 };
}

ast::nodes::array & heap::get_array ( const ast::nodes::arr_ref & ref )
{
    assert( ref . heap_idx < _array_pool . size() && "Heap error: invalid array reference.");
    return _array_pool [ ref.heap_idx ];
}

const ast::nodes::array & heap::get_array ( const ast::nodes::arr_ref & ref ) const
{
    assert( ref . heap_idx < _array_pool . size() && "Heap error: invalid array reference.");
    return _array_pool [ ref.heap_idx ];
}
