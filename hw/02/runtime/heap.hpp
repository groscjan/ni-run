//
// Created by Jan Groschaft on 3/7/21.
//

#ifndef INC_02_HEAP_HPP
#define INC_02_HEAP_HPP

#include "ast/nodes/object.hpp"
#include "ast/nodes/obj_ref.hpp"
#include "ast/nodes/arr_ref.hpp"
#include "ast/nodes/array.hpp"

#include <vector>

class heap
{
    std::vector<ast::nodes::object> _object_pool {};
    std::vector<ast::nodes::array> _array_pool {};
public:
    [[nodiscard]] ast::nodes::obj_ref alloc ( ast::nodes::object o );
    [[nodiscard]] ast::nodes::object& get ( const ast::nodes::obj_ref & ref );
    [[nodiscard]] const ast::nodes::object& get ( const ast::nodes::obj_ref & ref ) const;
    [[nodiscard]] ast::nodes::arr_ref alloc_array ( ast::nodes::array arr );
    [[nodiscard]] ast::nodes::array& get_array ( const ast::nodes::arr_ref & ref );
    [[nodiscard]] const ast::nodes::array& get_array ( const ast::nodes::arr_ref & ref ) const;
};


#endif //INC_02_HEAP_HPP
