//
// Created by Jan Groschaft on 3/4/21.
//

#ifndef INC_02_TOP_HPP
#define INC_02_TOP_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct top
    {
        std::vector <node> body;
    };
}

#endif //INC_02_TOP_HPP
