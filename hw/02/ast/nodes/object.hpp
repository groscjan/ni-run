//
// Created by Jan Groschaft on 3/8/21.
//

#ifndef INC_02_OBJECT_HPP
#define INC_02_OBJECT_HPP

#include "ast/node.hpp"
#include "runtime/stack_frame.hpp"

namespace ast::nodes
{
    struct object
    {
        node parent;
        stack_frame object_frame {};
    };
}

#endif //INC_02_OBJECT_HPP
