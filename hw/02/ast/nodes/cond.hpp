//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_COND_HPP
#define INC_02_COND_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct cond
    {
        node expr;
        node consequent;
        node alternative;
    };
}

#endif //INC_02_COND_HPP
