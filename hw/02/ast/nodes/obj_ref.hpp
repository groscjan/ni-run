//
// Created by Jan Groschaft on 3/7/21.
//

#ifndef INC_02_OBJ_REF_HPP
#define INC_02_OBJ_REF_HPP

#include <cstdint>

namespace ast::nodes
{
    struct obj_ref
    {
        std::size_t heap_idx;
    };
}

#endif //INC_02_OBJ_REF_HPP
