//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_BOOLEAN_HPP
#define INC_02_BOOLEAN_HPP

namespace ast::nodes
{
    struct boolean
    {
        bool val { false };
    };
}
#endif //INC_02_BOOLEAN_HPP
