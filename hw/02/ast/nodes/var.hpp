//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_VAR_HPP
#define INC_02_VAR_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct var
    {
        std::string name;
        node val;
    };
}

#endif //INC_02_VAR_HPP
