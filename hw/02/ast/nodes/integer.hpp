//
// Created by Jan Groschaft on 3/2/21.
//

#ifndef INC_02_INTEGER_HPP
#define INC_02_INTEGER_HPP

#include <cstdint>

namespace ast::nodes
{
    struct integer
    {
        int32_t val { 0 };
    };
}

#endif //INC_02_INTEGER_HPP
