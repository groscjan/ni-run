//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_FN_CALL_HPP
#define INC_02_FN_CALL_HPP

#include "ast/node.hpp"

#include <string>
#include <vector>

namespace ast::nodes
{
    struct fn_call
    {
        std::string name;
        std::vector<node> args;
    };
}

#endif //INC_02_FN_CALL_HPP
