//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_PRINT_HPP
#define INC_02_PRINT_HPP

#include "ast/node.hpp"

#include <vector>
#include <string>

namespace ast::nodes
{
    struct print
    {
        static constexpr char escape_symbol = '~';

        std::string format;
        std::vector<node> args;
    };
}

#endif //INC_02_PRINT_HPP
