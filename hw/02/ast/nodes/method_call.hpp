//
// Created by Jan Groschaft on 3/5/21.
//

#ifndef INC_02_METHOD_CALL_HPP
#define INC_02_METHOD_CALL_HPP

#include "ast/node.hpp"

#include <vector>

namespace ast::nodes
{
    struct method_call
    {
        node object;
        std::string name;
        std::vector<node> args;
    };
}

#endif //INC_02_METHOD_CALL_HPP
