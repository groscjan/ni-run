//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_VAR_ASSIGNMENT_HPP
#define INC_02_VAR_ASSIGNMENT_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct var_assignment
    {
        std::string identifier;
        node expr;
    };
}

#endif //INC_02_VAR_ASSIGNMENT_HPP
