//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_VAR_ACCESS_HPP
#define INC_02_VAR_ACCESS_HPP

namespace ast::nodes
{
    struct var_access
    {
        std::string name;
    };
}

#endif //INC_02_VAR_ACCESS_HPP
