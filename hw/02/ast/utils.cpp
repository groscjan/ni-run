//
// Created by Jan Groschaft on 3/3/21.
//


#include "utils.hpp"
#include "node.hpp"
#include "nodes/method_call.hpp"
#include "runtime/runtime.hpp"

#include <numeric>
#include <string>

using namespace std::literals::string_literals;

namespace ast::utils
{

    template <>
    [[nodiscard]] bool evaluate_truth_value ( const nodes::boolean & val )
    {
        return val . val;
    }

    template <>
    [[nodiscard]] bool evaluate_truth_value ( const nodes::null & val )
    {
        return false;
    }

    template <>
    std::string node_to_string ( const nodes::integer & val, [[maybe_unused]] const runtime * runtime_info )
    {
        return std::to_string ( val . val );
    }

    template <>
    std::string node_to_string ( const nodes::boolean & val, [[maybe_unused]] const runtime * runtime_info )
    {
        return val . val ? "true" : "false";
    }

    template <>
    std::string node_to_string ( const nodes::arr_ref & val, const runtime * runtime_info )
    {
        if ( runtime_info == nullptr )
            return "array_reference";
        const auto & arr = runtime_info -> get_array ( val );
        auto res = std::string { '[' };

        res += std::accumulate ( std::begin ( arr . data ), std::end ( arr . data ), std::string { },
                                 [ runtime_info ] ( std::string & context, const node & current )
                                 {
                                     return context . empty () ? current . to_str ( runtime_info ) : context + ", " +
                                                                                                     current . to_str ( runtime_info );
                                 } );

        return res + ']';
    }

    template <>
    std::string node_to_string ( const nodes::obj_ref & val, const runtime * runtime_info )
    {
        if ( runtime_info == nullptr )
            return "object_reference";
        const auto & obj = runtime_info -> get_object( val );

        auto key_value = [runtime_info] (const auto & pair)
        {
            return pair . first + '=' + pair . second . to_str ( runtime_info );
        };

        auto header = "object(..="s + obj . parent . to_str(runtime_info);
        auto res = std::accumulate ( std::begin ( obj . object_frame . get_variables() ), std::end ( obj . object_frame . get_variables() ), header,
                                 [ &key_value ] ( std::string & context, const auto & current )
                                 {
                                     return context + ", " + key_value ( current );
                                 } );

        return res + ')';
    }

    template <>
    std::string node_to_string ( const nodes::null & val, [[maybe_unused]] const runtime * runtime_info )
    {
        return "null";
    }

    template <>
    bool is_built_in_type ( const nodes::integer & val )
    {
        return true;
    }

    template <>
    bool is_built_in_type ( const nodes::boolean & val )
    {
        return true;
    }

    template <>
    bool is_built_in_type ( const nodes::null & val )
    {
        return true;
    }


    template <>
    node dispatch_built_in_type ( const nodes::integer & val, const nodes::method_call & call )
    {
        if ( call . args . size () != 1 )
            throw std::runtime_error ( "There is no method on the integer type that takes "s +
                                       std::to_string ( call . args . size () ) + " arguments." );
        const auto * rhs = call . args[0] . try_get_raw<nodes::integer> ();
        if ( call . name == "==" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { false } } : ast::node {
                    ast::nodes::boolean { val . val == rhs -> val } };
        if ( call . name == "!=" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { true } } : ast::node {
                    ast::nodes::boolean { val . val != rhs -> val } };

        if ( rhs == nullptr )
            throw std::runtime_error ("Invalid operand: integer expected as an operand to "s + call . name );

        if ( call . name == "+" )
            return ast::node { ast::nodes::integer { val . val + rhs -> val } };
        if ( call . name == "-" )
            return ast::node { ast::nodes::integer { val . val - rhs -> val } };
        if ( call . name == "*" )
            return ast::node { ast::nodes::integer { val . val * rhs -> val } };
        if ( call . name == "/" )
            return ast::node { ast::nodes::integer { val . val / rhs -> val } };
        if ( call . name == "%" )
            return ast::node { ast::nodes::integer { val . val % rhs -> val } };
        if ( call . name == "<" )
            return ast::node { ast::nodes::boolean { val . val < rhs -> val } };
        if ( call . name == ">" )
            return ast::node { ast::nodes::boolean { val . val > rhs -> val } };
        if ( call . name == "<=" )
            return ast::node { ast::nodes::boolean { val . val <= rhs -> val } };
        if ( call . name == ">=" )
            return ast::node { ast::nodes::boolean { val . val >= rhs -> val } };
        throw std::runtime_error ( "Unsupported integer method: "s + call . name );
    }

    template <>
    node dispatch_built_in_type ( const nodes::boolean & val, const nodes::method_call & call )
    {
        if ( call . args . size () != 1 )
            throw std::runtime_error ( "There is no method on the boolean type that takes "s +
                                       std::to_string ( call . args . size () ) + " arguments." );
        const auto * rhs = call . args[0] . try_get_raw<ast::nodes::boolean> ();
        if ( call . name == "==" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { false } } : ast::node {
                    ast::nodes::boolean { val . val == rhs -> val } };
        if ( call . name == "!=" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { true } } : ast::node {
                    ast::nodes::boolean { val . val != rhs -> val } };

        if ( rhs == nullptr )
            throw std::runtime_error ( "Unsupported boolean method operand. Boolean expected." );

        if ( call . name == "&" )
            return ast::node { ast::nodes::boolean { val . val && rhs -> val } };
        if ( call . name == "|" )
            return ast::node { ast::nodes::boolean { val . val || rhs -> val } };
        throw std::runtime_error ( "Unsupported boolean method: "s + call . name );
    }

    template <>
    node dispatch_built_in_type ( const nodes::null & val, const nodes::method_call & call )
    {
        if ( call . args . size () != 1 )
            throw std::runtime_error ( "There is no method on the null type that takes "s +
                                       std::to_string ( call . args . size () ) + " arguments." );
        const auto * rhs = call . args[0] . try_get_raw<ast::nodes::null> ();

        if ( call . name == "==" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { false } } : ast::node {
                    ast::nodes::boolean { true } };
        if ( call . name == "!=" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { true } } : ast::node {
                    ast::nodes::boolean { false } };

        throw std::runtime_error ( "Unsupported null method: "s + call . name );
    }
}
