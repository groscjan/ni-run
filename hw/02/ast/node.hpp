//
// Created by Jan Groschaft on 3/2/21.
//

#ifndef INC_02_NODE_HPP
#define INC_02_NODE_HPP

#include "visitors/visitor.hpp"
#include "utils.hpp"


#include <cstdint>
#include <memory>
#include <typeinfo>
#include <string>

class runtime;

using namespace std::literals::string_literals;


namespace ast
{
    class node
    {
    public:
        template <typename T, typename Allocator = std::allocator<T>>
        explicit node (T val, const Allocator & a = Allocator()) : _data ( std::allocate_shared<model<T>> ( a, std::move ( val ) ) )
        {
        }

        node accept ( visitor * v ) const
        {
            return _data -> accept ( v, *this );
        }

        [[nodiscard]] bool truth_value ( ) const
        {
            return _data -> truth_value ();
        }

        [[nodiscard]] std::string to_str ( const runtime * runtime_info ) const
        {
            return _data -> to_str ( runtime_info );
        }

        [[nodiscard]] bool is_built_in_type ( ) const
        {
            return _data -> is_built_in_type ();
        }

        [[nodiscard]] node dispatch_built_in_type ( const nodes::method_call & call ) const
        {
            return _data -> dispatch_built_in_type ( call );
        }

        template <typename T>
        const T * try_get_raw ( ) const
        {
            const auto * m = dynamic_cast<const model<T>*>(_data . get());
            return m == nullptr ? nullptr : &(m -> data);
        }

        template <typename T>
        const T * get_raw ( ) const
        {
            const auto * m = dynamic_cast<const model<T>*>(_data . get());
            if ( m == nullptr )
                throw std::runtime_error ( "Type error: expected "s + typeid(T).name() + ", found " + utils::node_to_string(*(_data . get ()), nullptr));
            return &(m -> data);
        }

    private:
        struct concept_t
        {
            virtual ~concept_t() = default;

            virtual node accept ( visitor * v, const node & container ) const = 0;

            [[nodiscard]] virtual bool truth_value ( ) const = 0;

            [[nodiscard]] virtual std::string to_str ( const runtime * runtime_info ) const = 0;

            [[nodiscard]] virtual bool is_built_in_type ( ) const = 0;

            [[nodiscard]] virtual node dispatch_built_in_type ( const nodes::method_call & call ) const = 0;
        };


        template <typename T>
        struct model final : concept_t
        {
            explicit model(T val) : data ( std::move ( val ) )
            {
            }

            node accept ( visitor * v, const node & container ) const override;

            [[nodiscard]] bool truth_value ( ) const override;

            [[nodiscard]] std::string to_str ( const runtime * runtime_info ) const override;

            [[nodiscard]] bool is_built_in_type ( ) const override;

            [[nodiscard]] node dispatch_built_in_type ( const nodes::method_call & call ) const override;

            T data;
        };

        std::shared_ptr<const concept_t> _data;
    };
}

namespace ast::utils
{
    template <typename T>
    node dispatch_built_in_type ( const T & val, const nodes::method_call & call )
    {
        throw std::runtime_error ( "Attempting to dispatch on non-built-in type.");
    }

    template <>
    node dispatch_built_in_type ( const nodes::integer & val, const nodes::method_call & call );

    template <>
    node dispatch_built_in_type ( const nodes::boolean & val, const nodes::method_call & call );

    template <>
    node dispatch_built_in_type ( const nodes::null & val, const nodes::method_call & call );
}

template <typename T>
bool ast::node::model<T>::truth_value ( ) const
{
    return ast::utils::evaluate_truth_value(data);
}

template <typename T>
std::string ast::node::model<T>::to_str ( const runtime * runtime_info ) const
{
    return ast::utils::node_to_string ( data, runtime_info );
}

template <typename T>
ast::node ast::node::model<T>::accept ( visitor * v, const ast::node & container ) const
{
    return v->visit(data, container);
}

template <typename T>
bool ast::node::model<T>::is_built_in_type ( ) const
{
    return ast::utils::is_built_in_type ( data );
}

template <typename T>
ast::node ast::node::model<T>::dispatch_built_in_type ( const ast::nodes::method_call & call ) const
{
    return ast::utils::dispatch_built_in_type ( data, call );
}


#endif //INC_02_NODE_HPP
