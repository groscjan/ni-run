//
// Created by Jan Groschaft on 3/3/21.
//

#include "interpreter.hpp"
#include "ast/nodes/block.hpp"
#include "ast/nodes/array.hpp"
#include "ast/nodes/boolean.hpp"
#include "ast/nodes/cond.hpp"
#include "ast/nodes/fn_call.hpp"
#include "ast/nodes/fn_decl.hpp"
#include "ast/nodes/integer.hpp"
#include "ast/nodes/loop.hpp"
#include "ast/nodes/null.hpp"
#include "ast/nodes/print.hpp"
#include "ast/nodes/var.hpp"
#include "ast/nodes/var_access.hpp"
#include "ast/nodes/var_assignment.hpp"
#include "ast/nodes/top.hpp"
#include "ast/nodes/field_access.hpp"
#include "ast/nodes/field_assignment.hpp"
#include "ast/nodes/obj_def.hpp"
#include "ast/nodes/method_call.hpp"
#include "ast/nodes/object.hpp"
#include "ast/nodes/arr_def.hpp"
#include "ast/nodes/arr_assignment.hpp"
#include "ast/nodes/arr_access.hpp"
#include "ast/nodes/arr_ref.hpp"
#include "ast/nodes/array.hpp"


#include <iostream>
#include <algorithm>


auto visit_array ( visitor * v, const std::vector<ast::node> & arr )
{
    auto visited = std::vector<ast::node> { };
    visited . reserve ( arr . size () );
    std::transform ( std::begin ( arr ), std::end ( arr ), std::back_inserter ( visited ),
                     [ v ] ( const ast::node & n )
                     { return n . accept ( v ); } );
    return visited;
}




ast::node interpreter::visit ( const ast::nodes::top & top, const ast::node & container )
{
    _runtime . push_base_frame ();
    auto ret = ast::node { ast::nodes::null { } };
    for ( const auto & statement : top . body )
        ret = statement . accept ( this );
    _runtime . pop_frame ();
    return ret;
}

ast::node interpreter::visit ( const ast::nodes::block & blk, const ast::node & container )
{
    _runtime . push_frame ();
    auto ret = ast::node { ast::nodes::null { } };
    for ( const auto & statement : blk . body )
        ret = statement . accept ( this );
    _runtime . pop_frame ();
    return ret;
}

ast::node interpreter::visit ( const ast::nodes::boolean & b, const ast::node & container )
{
    return container;
}

ast::node interpreter::visit ( const ast::nodes::cond & con, const ast::node & container )
{
    const auto c = con . expr . accept ( this );
    if ( c . truth_value () )
        return con . consequent . accept ( this );
    return con . alternative . accept ( this );
}

ast::node interpreter::visit ( const ast::nodes::fn_call & fn_c, const ast::node & container )
{
    auto * fn = _runtime . get_current_frame () . get_function ( fn_c . name ) . get_raw<ast::nodes::fn_decl> ();

    if ( fn -> params . size () != fn_c . args . size () )
        throw std::runtime_error ( fn -> name + " expects " + std::to_string ( fn -> params . size () ) +
                                   " parameters, but " + std::to_string ( fn_c . args . size () ) +
                                   " arguments were provided." );


    auto visited_args = visit_array ( this, fn_c . args );
    auto & local_frame = _runtime . push_frame ( true );
    for ( std::size_t i = 0; i < fn -> params . size (); ++i )
        local_frame . create_variable ( fn -> params[i], visited_args[i] );

    auto ret = fn -> body . accept ( this );
    _runtime . pop_frame ();
    return ret;
}

ast::node interpreter::visit ( const ast::nodes::fn_decl & fn_d, const ast::node & container )
{
    auto & frame = _runtime . get_current_frame ();
    frame . create_function ( fn_d . name, container );
    return ast::node ( ast::nodes::null { } );
}

ast::node interpreter::visit ( const ast::nodes::integer & i, const ast::node & container )
{
    return container;
}

ast::node interpreter::visit ( const ast::nodes::loop & l, const ast::node & container )
{
    auto res = ast::node ( ast::nodes::null { } );
    while ( l . cond . accept ( this ) . truth_value () )
        res = l . body . accept ( this );
    return res;
}

ast::node interpreter::visit ( const ast::nodes::null & n, const ast::node & container )
{
    return container;
}

ast::node interpreter::visit ( const ast::nodes::print & p, const ast::node & container )
{
    auto visited_args = visit_array(this, p . args);

    auto arg_idx = 0ul;
    for ( auto i = 0ul; i < p . format . size (); ++i )
    {
        if ( i == p . format . size () )
            break;

        const auto c = p . format[i];
        if ( c == ast::nodes::print::escape_symbol )
        {
            if ( arg_idx >= p . args . size () )
                throw std::runtime_error ( "Too few arguments to print call." );
            const auto & arg = visited_args[arg_idx++];
            std::cout << arg . to_str ( &_runtime );
        } else if ( c == '\\' )
        {
            if ( ++i == p . format . size () )
                throw std::runtime_error ( "Symbol expected after '\\'" );
            switch ( p . format[i] )
            {
                case '~':
                    std::cout << "~";
                    break;
                case 'n':
                    std::cout << '\n';
                    break;
                case '"':
                    std::cout << '\"';
                    break;
                case 'r':
                    std::cout << '\r';
                    break;
                case 't':
                    std::cout << '\t';
                    break;
                case '\\':
                    std::cout << '\\';
                    break;
                default:
                    throw std::runtime_error ( "Invalid symbol after escape: "s + p . format[i] );
            }
        } else
        {
            std::cout << c;
        }
    }

    return ast::node ( ast::nodes::null { } );
}

ast::node interpreter::visit ( const ast::nodes::var_access & var_access, const ast::node & container )
{
    auto & frame = _runtime . get_current_frame ();
    return frame . get_variable ( var_access . name );
}

ast::node interpreter::visit ( const ast::nodes::var_assignment & var_assign, const ast::node & container )
{
    auto & frame = _runtime . get_current_frame ();
    auto val = var_assign . expr . accept ( this );
    frame . assign_variable ( var_assign . identifier, val );
    return val;
}

ast::node interpreter::visit ( const ast::nodes::var & var, const ast::node & container )
{
    auto & frame = _runtime . get_current_frame ();
    auto val = var . val . accept ( this );
    frame . create_variable ( var . name, val );
    return val;
}

ast::node interpreter::visit ( const ast::nodes::obj_def & obj, const ast::node & container )
{
    auto new_obj = ast::nodes::object { obj . extends . accept ( this ) };

    _runtime . push_frame ();
    for ( const auto & member : obj . members )
        member . accept ( this );
    new_obj . object_frame = _runtime . pop_frame ();

    auto obj_ref = _runtime . alloc ( std::move ( new_obj ) );
    return ast::node { obj_ref };
}

ast::node interpreter::visit ( const ast::nodes::obj_ref & ref, const ast::node & container )
{
    return container;
}

ast::node interpreter::visit ( const ast::nodes::field_access & field_access, const ast::node & container )
{
    const auto * obj_ref = field_access . object . accept ( this ) . get_raw<ast::nodes::obj_ref> ();
    auto & obj = _runtime . get_object ( *obj_ref );
    return obj . object_frame . get_variable ( field_access . field );
}

ast::node interpreter::visit ( const ast::nodes::field_assignment & field_assignment, const ast::node & container )
{
    const auto * obj_ref = field_assignment . object . accept ( this ) . get_raw<ast::nodes::obj_ref> ();
    auto & obj = _runtime . get_object ( *obj_ref );
    auto val = field_assignment . expr . accept ( this );
    obj . object_frame . assign_variable ( field_assignment . field, val );
    return val;
}

ast::node interpreter::visit ( const ast::nodes::method_call & m_call, const ast::node & container )
{
    auto parsed_call = ast::nodes::method_call { m_call . object . accept ( this ), m_call . name,
                                                 visit_array ( this, m_call . args ) };
    if ( parsed_call . object . is_built_in_type () )
        return parsed_call . object . dispatch_built_in_type ( parsed_call );

    const auto * obj_ref = parsed_call . object . get_raw<ast::nodes::obj_ref> ();
    auto & obj = _runtime . get_object ( *obj_ref );

    if ( !obj . object_frame . has_function ( m_call . name ) )
    {
        auto parent_call = ast::node { ast::nodes::method_call { obj . parent, std::move ( parsed_call . name ),
                                                                 std::move ( parsed_call . args ) } };
        return parent_call . accept ( this );
    }

    const auto * method = obj . object_frame . get_function ( m_call . name ) . get_raw<ast::nodes::fn_decl> ();
    if ( method -> params . size () != m_call . args . size () )
        throw std::runtime_error ( method -> name + " expects " + std::to_string ( method -> params . size () ) +
                                   " parameters, but " + std::to_string ( m_call . args . size () ) +
                                   " arguments were provided." );

    auto & base_object_frame = _runtime . push_frame ( true );
    base_object_frame . create_variable ( "this", parsed_call . object );
    for ( std::size_t i = 0; i < method -> params . size (); ++i )
        base_object_frame . create_variable ( method -> params[i], parsed_call . args [i] );

    auto ret = method -> body . accept ( this );
    _runtime . pop_frame ();
    return ret;
}

ast::node interpreter::visit ( const ast::nodes::arr_def & arr, const ast::node & container )
{
    auto size = arr . size . accept(this) . get_raw<ast::nodes::integer>() -> val;
    auto elems = std::vector<ast::node> {};
    elems . reserve(size);
    std::generate_n(std::back_inserter(elems), size, [&arr, this] ()
    {
        this->_runtime.push_frame();
        auto ret = arr.value.accept(this);
        this->_runtime.pop_frame();
        return ret;
    });

    auto array = ast::nodes::array { std::move ( elems ) };
    auto arr_ref = _runtime . alloc_array( std::move ( array ) );
    return ast::node { arr_ref };
}

ast::node interpreter::visit ( const ast::nodes::arr_assignment & arr_assign, const ast::node & container )
{
    auto parsed = arr_assign . array . accept ( this );
    if ( auto * obj_ref = parsed . try_get_raw<ast::nodes::obj_ref> ())
    {
        auto call = ast::node { ast::nodes::method_call
                { ast::node { *obj_ref },
                  "set",
                  { arr_assign . index, arr_assign . value }
                } };
        return call . accept ( this );
    }

    auto ref = arr_assign . array . accept ( this ) . get_raw<ast::nodes::arr_ref>();
    std::size_t idx = arr_assign . index . accept( this ) . get_raw<ast::nodes::integer>() -> val;
    auto val = arr_assign . value . accept( this );

    auto & arr = _runtime . get_array( *ref );

    if ( idx >= std::size ( arr . data ) )
        throw std::runtime_error ( "Index "s + std::to_string(idx) + " out of bounds for array access." );

    arr . data[idx] = val;
    return val;
}

ast::node interpreter::visit ( const ast::nodes::arr_access & arr_access, const ast::node & container )
{
    auto parsed = arr_access . array . accept ( this );
    if ( auto * obj_ref = parsed . try_get_raw<ast::nodes::obj_ref> ())
    {
        auto call = ast::node { ast::nodes::method_call
                                        { ast::node { *obj_ref },
                                          "get",
                                          { arr_access . index }
                                        } };
        return call . accept ( this );
    }


    auto ref = arr_access . array . accept ( this ) . get_raw<ast::nodes::arr_ref>();
    std::size_t idx = arr_access . index . accept( this ) . get_raw<ast::nodes::integer>() -> val;
    auto & arr = _runtime . get_array( *ref );

    if ( idx >= std::size ( arr . data ) )
        throw std::runtime_error ( "Index "s + std::to_string(idx) + " out of bounds for array access." );

    return arr . data[idx];
}

ast::node interpreter::visit ( const ast::nodes::arr_ref & ref, const ast::node & container )
{
    return container;
}






