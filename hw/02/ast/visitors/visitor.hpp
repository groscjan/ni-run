//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_VISITOR_HPP
#define INC_02_VISITOR_HPP


namespace ast
{
    class node;
}

namespace ast::nodes
{
    struct array;
    struct block;
    struct boolean;
    struct cond;
    struct fn_call;
    struct fn_decl;
    struct integer;
    struct loop;
    struct null;
    struct print;
    struct var_access;
    struct var_assignment;
    struct var;
    struct top;
    struct obj_def;
    struct obj_ref;
    struct field_access;
    struct field_assignment;
    struct method_call;
    struct array;
    struct arr_def;
    struct arr_access;
    struct arr_assignment;
    struct arr_ref;
}

class visitor
{
public:
    virtual ~visitor() = default;
    virtual ast::node visit ( const ast::nodes::block & blk, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::top & blk, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::boolean & b, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::cond & con, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::fn_call & fn_c, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::fn_decl & fn_d, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::integer & i, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::loop & l, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::null & n, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::print & p, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::var_access & var_access, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::var_assignment & var_assign, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::var & var, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::obj_def & obj, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::obj_ref & ref, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::field_access & field_access, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::field_assignment & field_assignment, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::method_call & m_call, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::arr_def & arr, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::arr_assignment & arr_assign, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::arr_access & arr_access, const ast::node & container ) = 0;
    virtual ast::node visit ( const ast::nodes::arr_ref & ref, const ast::node & container ) = 0;

};

#endif //INC_02_VISITOR_HPP
