//
// Created by Jan Groschaft on 3/12/21.
//

#ifndef INC_03_TYPES_HPP
#define INC_03_TYPES_HPP

#include "program_objects.hpp"
#include "instructions.hpp"
#include "runtime_objects.hpp"

#include <vector>
#include <algorithm>

namespace fml
{
    struct program
    {
        std::vector<types::program_object> constant_pool {};
        std::vector<types::index_t> globals {};
        uint16_t entry_point {};
        types::code instructions {};

        types::index_t register_program_object ( types::program_object obj )
        {
            if ( const auto it = std::find ( std::begin ( constant_pool), std::end ( constant_pool ), obj )
                    ; it != std::end ( constant_pool ) )
                return std::distance ( std::begin ( constant_pool ), it );

            const auto ret = constant_pool . size ();
            constant_pool . push_back ( std::move ( obj ) );
            return ret;
        }

        types::index_t register_global ( types::index_t index )
        {
            const auto ret = globals . size();
            globals . push_back ( index );
            return ret;
        }

        uint32_t register_code ( const types::code & new_code )
        {
            const auto ret = instructions . data . size();
            instructions . data . insert ( std::end( instructions . data ), std::begin ( new_code . data ), std::end ( new_code . data ) );
            return ret;
        }
    };
}
#endif //INC_03_TYPES_HPP
