//
// Created by Jan Groschaft on 3/14/21.
//

#ifndef INC_03_INSTRUCTIONS_HPP
#define INC_03_INSTRUCTIONS_HPP

#include "serialization/utils.hpp"
#include "serialization/opcodes.hpp"
#include "serialization/memory_writer.hpp"

#include <variant>
#include <cstdint>
#include <vector>

namespace fml
{
    using instruction_ptr = uint32_t;
}

namespace fml::types
{
    using index_t = uint16_t;

    struct literal
    {
        index_t idx;
    };

    struct get_loc
    {
        index_t idx;
    };

    struct set_loc
    {
        index_t idx;
    };


    struct get_glob
    {
        index_t idx;
    };

    struct set_glob
    {
        index_t idx;
    };

    struct fn_call
    {
        index_t idx;
        uint8_t arg_cnt;
    };

    struct ret
    {
    };

    struct label
    {
        index_t idx;
    };

    struct jump
    {
        index_t idx;
    };

    struct branch
    {
        index_t idx;
    };


    struct print
    {
        index_t idx;
        uint8_t arg_cnt;
    };

    struct arr_def
    {
    };

    struct obj_def
    {
        index_t idx;
    };

    struct get_field
    {
        index_t idx;
    };

    struct set_field
    {
        index_t idx;
    };

    struct m_call
    {
        index_t idx;
        uint8_t arg_cnt;
    };

    struct drop
    {
    };


    using instruction = std::variant<literal, get_loc, set_loc, get_glob, set_glob,
        fn_call, ret, label, jump, branch, print, arr_def, obj_def, get_field, set_field, m_call, drop>;


    struct code_binary
    {
        std::vector<std::byte> data;

        void emit ( instruction i )
        {
            std::visit(overloaded {
                    [this] (literal i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::literal) );
                        utils::write_integral ( i . idx, data );
                        },
                    [this] (get_loc i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::get_loc) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (set_loc i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::set_loc) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (get_glob i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::get_glob) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (set_glob i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::set_glob) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (fn_call i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::call_fn) );
                        utils::write_integral ( i . idx, data );
                        utils::write_integral ( i . arg_cnt, data );
                    },
                    [this] (ret i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::ret) );
                    },
                    [this] (label i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::label) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (jump i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::jump) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (branch i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::branch) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (print i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::print) );
                        utils::write_integral ( i . idx, data );
                        utils::write_integral ( i . arg_cnt, data );
                    },
                    [this] (arr_def i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::arr) );
                    },
                    [this] (obj_def i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::object) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (set_field i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::set_field) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (get_field i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::get_field) );
                        utils::write_integral ( i . idx, data );
                    },
                    [this] (m_call i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::call_m) );
                        utils::write_integral ( i . idx, data );
                        utils::write_integral ( i . arg_cnt, data );
                    },
                    [this] (drop i) {
                        data . push_back( static_cast<std::byte>(serializations::instruction_opcode::drop) );
                    },
            }, i);
        }
    };

    struct code_str
    {
        std::vector<instruction> data;

        void emit ( instruction i )
        {
            data . push_back ( i );
        }
    };

    using code = code_str;
}


#endif //INC_03_INSTRUCTIONS_HPP
