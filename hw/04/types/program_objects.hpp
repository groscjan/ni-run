//
// Created by Jan Groschaft on 3/12/21.
//

#ifndef INC_03_PROGRAM_OBJECTS_HPP
#define INC_03_PROGRAM_OBJECTS_HPP

#include "instructions.hpp"

#include <cstdint>
#include <vector>
#include <variant>
#include <string>


namespace fml::types
{
    struct integer
    {
        int val;
        bool operator <=> ( const integer & ) const = default;
    };

    struct boolean
    {
        bool val;
        bool operator <=> ( const boolean & ) const = default;
    };

    struct null
    {
        bool operator <=> ( const null & ) const = default;
    };

    struct string
    {
        std::string str;
        bool operator <=> ( const string & ) const = default;
    };

    struct slot
    {
        index_t idx;
        bool operator <=> ( const slot & ) const = default;
    };

    struct method
    {
        index_t idx;
        uint32_t code_start_idx;
        uint32_t len;
        uint16_t locals;
        uint8_t args;

        bool operator <=> ( const method & ) const = default;
    };


    struct class_t
    {
        std::vector <index_t> fields;

        bool operator <=> ( const class_t & ) const = default;
    };

    using program_object = std::variant<integer, boolean, null, string ,slot, method, class_t>;
}

#endif //INC_03_PROGRAM_OBJECTS_HPP
