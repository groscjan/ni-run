//
// Created by Jan Groschaft on 5/22/21.
//

#ifndef INC_04_PROGRAM_SERIALIZER_HPP
#define INC_04_PROGRAM_SERIALIZER_HPP

#include "types/types.hpp"
#include "memory_writer.hpp"

#include <fstream>

namespace fml::serialization
{
    void serialize ( const types::program_object & prog_obj, const types::code & code_vec, types::code_binary & target )
    {
        using namespace types;
        std::visit(overloaded {
                [ &target ] ( integer i )
                {
                    target . data . push_back ( static_cast<std::byte>(serializations::program_object_opcode::integer) );
                    utils::write_integral ( i . val, target . data );
                },
                [ &target ] ( boolean i )
                {
                    target . data . push_back ( static_cast<std::byte>(serializations::program_object_opcode::boolean) );
                    utils::write_integral ( i . val, target . data );
                },
                [ &target ] ( null i )
                {
                    target . data . push_back ( static_cast<std::byte>(serializations::program_object_opcode::null) );
                },
                [ &target ] ( const string & i )
                {
                    target . data . push_back ( static_cast<std::byte>(serializations::program_object_opcode::string) );
                    utils::write_string( i . str, target . data );
                },
                [ &target ] ( slot i )
                {
                    target . data . push_back ( static_cast<std::byte>(serializations::program_object_opcode::slot) );
                    utils::write_integral ( i . idx , target . data );
                },
                [ &target, &code_vec ] ( method m )
                {
                    target . data . push_back ( static_cast<std::byte>(serializations::program_object_opcode::method) );
                    utils::write_integral ( m . idx, target . data );
                    utils::write_integral ( m . args, target . data );
                    utils::write_integral ( m . locals, target . data );
                    utils::write_integral ( m . len, target . data );
                    for ( auto idx = 0u; idx < m . len; ++idx )
                        target . emit ( code_vec . data [idx + m . code_start_idx] );
                },
                [ &target ] ( const class_t & i )
                {
                    target . data . push_back ( static_cast<std::byte>(serializations::program_object_opcode::class_t) );
                    utils::write_integral ( (uint16_t) i . fields . size(), target . data );
                    for ( auto idx : i . fields )
                        utils::write_integral ( idx, target . data );
                },
        }, prog_obj );
    }



    void serialize_to_bc ( const program & prog, std::ostream & target )
    {
        auto bc = types::code_binary {};
        utils::write_integral ( (uint16_t) prog . constant_pool . size(), bc . data );
        for ( const auto & prog_obj : prog . constant_pool )
            serialize ( prog_obj, prog . instructions, bc );
        utils::write_integral ( (uint16_t) prog . globals . size(), bc . data );
        for ( auto idx : prog . globals )
            utils::write_integral ( idx, bc . data );
        utils::write_integral ( prog . entry_point, bc . data );

        target . write ( reinterpret_cast<const char *> ( bc . data . data() ), bc . data . size() );
    }


}


#endif //INC_04_PROGRAM_SERIALIZER_HPP
