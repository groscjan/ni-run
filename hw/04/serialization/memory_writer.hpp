//
// Created by Jan Groschaft on 3/12/21.
//

#ifndef INC_04_MEMORY_WRITER_HPP
#define INC_04_MEMORY_WRITER_HPP

#include <cstdint>
#include <cstddef>
#include <vector>
#include <string>

// utils for writing data to memory stored as little-endian

namespace fml::utils
{
    template <std::integral type>
    void write_integral ( type val, std::vector<std::byte> & memory )
    {
        const auto size_bytes = sizeof (type);
        for ( auto i = 0ul; i < size_bytes; ++i )
            memory . push_back ( static_cast<std::byte> ( ( val >> ( 8 * i ) ) & 0xff ) );
    }

    void write_string ( const std::string & val, std::vector<std::byte> & memory );
}

#endif //INC_04_MEMORY_WRITER_HPP
