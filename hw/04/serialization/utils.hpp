//
// Created by Jan Groschaft on 5/21/21.
//

#ifndef INC_04_AST_UTILS_HPP
#define INC_04_AST_UTILS_HPP

#include <memory>

namespace fml
{
    template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
    template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;
}




#endif //INC_04_AST_UTILS_HPP
