//
// Created by Jan Groschaft on 5/21/21.
//

#ifndef INC_04_OPCODES_HPP
#define INC_04_OPCODES_HPP

namespace fml::serializations
{
    enum class instruction_opcode : uint8_t
    {
        literal = 0x01,
        get_loc = 0x0a,
        set_loc = 0x09,
        get_glob = 0x0c,
        set_glob = 0x0b,
        call_fn = 0x08,
        ret = 0x0f,
        label = 0x00,
        jump = 0x0e,
        branch = 0x0d,
        print = 0x02,
        arr = 0x03,
        object = 0x04,
        get_field = 0x05,
        set_field = 0x06,
        call_m = 0x07,
        drop = 0x10
    };

    enum class program_object_opcode : uint8_t
    {
        integer = 0x00,
        boolean = 0x06,
        null = 0x01,
        string = 0x02,
        slot = 0x04,
        method = 0x03,
        class_t = 0x05
    };
}

#endif //INC_04_OPCODES_HPP
