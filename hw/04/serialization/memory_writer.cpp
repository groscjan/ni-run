//
// Created by Jan Groschaft on 5/22/21.
//

#include "memory_writer.hpp"
namespace fml::utils
{
    void write_string ( const std::string & val, std::vector<std::byte> & memory )
    {
        fml::utils::write_integral( (uint32_t) val .size(), memory );
        for ( auto c : val )
            memory . push_back ( static_cast<std::byte> ( c ) );
    }
}