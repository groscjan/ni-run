//
// Created by Jan Groschaft on 3/4/21.
//

#include "json_ast_parser.hpp"

#include "rapidjson/istreamwrapper.h"
#include "rapidjson/document.h"
#include "ast/nodes/block.hpp"
#include "ast/nodes/fn_call.hpp"
#include "ast/nodes/block.hpp"
#include "ast/nodes/integer.hpp"
#include "ast/nodes/var.hpp"
#include "ast/nodes/var_access.hpp"
#include "ast/nodes/var_assignment.hpp"
#include "ast/nodes/fn_decl.hpp"
#include "ast/nodes/print.hpp"
#include "ast/nodes/loop.hpp"
#include "ast/nodes/cond.hpp"
#include "ast/nodes/top.hpp"
#include "ast/nodes/obj_def.hpp"
#include "ast/nodes/field_assignment.hpp"
#include "ast/nodes/field_access.hpp"
#include "ast/nodes/method_call.hpp"
#include "ast/nodes/boolean.hpp"
#include "ast/nodes/null.hpp"
#include "ast/nodes/array.hpp"
#include "ast/nodes/arr_def.hpp"
#include "ast/nodes/arr_access.hpp"
#include "ast/nodes/arr_assignment.hpp"


#include <fstream>
#include <vector>

std::vector<ast::node> parse_object_array ( const rapidjson::Value::ConstArray & array );

std::vector<std::string> parse_string_array ( const rapidjson::Value::ConstArray & array );

ast::node parse_object ( const rapidjson::Value & value );

ast::node parse_string ( std::string_view str );


auto get_json_doc ( std::ifstream & ifs )
{
    auto stream = rapidjson::IStreamWrapper { ifs };
    rapidjson::Document document;
    if ( auto err = document . ParseStream ( stream ) . GetParseError(); err != 0 )
        throw std::invalid_argument ( "AST parse error: invalid json."s + " Error code: " + std::to_string(err) );
    return document;
}


auto parse_child ( const rapidjson::Value & container, std::string_view child_name )
{
    return container[std::begin(child_name)].IsObject() ?
        parse_object(container[std::begin(child_name)].GetObject()) :
        parse_string(container[std::begin(child_name)].GetString());
}

ast::node parse_object ( const rapidjson::Value & value )
{
    if ( value . HasMember ( "Integer" ) )
    {
        auto i = value["Integer"] . GetInt ();
        return ast::node { ast::nodes::integer { i } };
    }
    if ( value . HasMember ( "Boolean" ) )
    {
        auto b = value["Boolean"] . GetBool ();
        return ast::node { ast::nodes::boolean { b } };
    }
    if ( value . HasMember ( "Variable" ) )
    {
        const auto & var = value["Variable"] . GetObject ();
        return ast::node { ast::nodes::var { var["name"] . GetString (),
                                             parse_child( var, "value" ) } };
    }
    if ( value . HasMember ( "AccessVariable" ) )
    {
        const auto & access = value["AccessVariable"] . GetObject ();
        return ast::node { ast::nodes::var_access { access["name"] . GetString () } };
    }
    if ( value . HasMember ( "AssignVariable" ) )
    {
        const auto & assign = value["AssignVariable"] . GetObject ();
        return ast::node { ast::nodes::var_assignment { assign["name"] . GetString (),
                                                        parse_child ( assign, "value" ) } };
    }
    if ( value . HasMember ( "Function" ) )
    {
        const auto & fn = value["Function"] . GetObject ();
        return ast::node { ast::nodes::fn_decl { fn["name"] . GetString (),
                                                 parse_string_array ( fn["parameters"] . GetArray () ),
                                                 parse_child( fn, "body" ) } };
    }
    if ( value . HasMember ( "CallFunction" ) )
    {
        const auto & fn = value["CallFunction"] . GetObject ();
        return ast::node { ast::nodes::fn_call { fn["name"] . GetString (),
                                                 parse_object_array ( fn["arguments"] . GetArray () ) } };
    }
    if ( value . HasMember ( "Print" ) )
    {
        const auto & print = value["Print"] . GetObject ();
        return ast::node { ast::nodes::print { print["format"] . GetString (),
                                               parse_object_array ( print["arguments"] . GetArray () ) } };
    }
    if ( value . HasMember ( "Block" ) )
    {
        const auto & blk = value["Block"] . GetArray();
        return ast::node { ast::nodes::block { parse_object_array( blk ) }};
    }
    if ( value . HasMember ( "Loop" ) )
    {
        const auto & loop = value["Loop"] . GetObject ();
        return ast::node { ast::nodes::loop { parse_child( loop, "condition" ),
                                              parse_child( loop, "body" )}};
    }
    if ( value . HasMember ( "Conditional" ) )
    {
        const auto & cond = value["Conditional"] . GetObject ();
        return ast::node { ast::nodes::cond { parse_child( cond, "condition" ),
                                              parse_child( cond, "consequent" ),
                                              parse_child( cond, "alternative" ) }};
    }
    if ( value . HasMember( "Object" ) )
    {
       const auto & obj = value["Object"] . GetObject();
       return ast::node { ast::nodes::obj_def { parse_child( obj, "extends" ),
                                               parse_object_array( obj["members"] . GetArray() ) } };
    }
    if ( value . HasMember( "AssignField" ) )
    {
        const auto & assign = value["AssignField"] . GetObject();
        return ast::node { ast::nodes::field_assignment { parse_child( assign, "object" ),
                                                           assign["field"] . GetString(),
                                                          parse_child( assign, "value" ) } };
    }
    if ( value . HasMember( "AccessField" ) )
    {
        const auto & access = value["AccessField"] . GetObject();
        return ast::node { ast::nodes::field_access { parse_child( access, "object" ),
                                                          access["field"] . GetString() } } ;
    }
    if ( value . HasMember( "CallMethod" ) )
    {
        const auto & call = value["CallMethod"] . GetObject();
        return ast::node { ast::nodes::method_call { parse_child( call, "object" ),
                                                     call["name"] . GetString(),
                                                     parse_object_array( call["arguments"] . GetArray() ) } } ;
    }
    if ( value . HasMember( "Array" ) )
    {
        const auto & arr = value["Array"] . GetObject();
        return ast::node { ast::nodes::arr_def { parse_child( arr, "size" ),
                                                     parse_child(arr, "value") } } ;
    }
    if ( value . HasMember( "AccessArray" ) )
    {
        const auto & access = value["AccessArray"] . GetObject();
        return ast::node { ast::nodes::arr_access { parse_child( access, "array" ),
                                                    parse_child( access, "index" ) } } ;
    }
    if ( value . HasMember( "AssignArray" ) )
    {
        const auto & assign = value["AssignArray"] . GetObject();
        return ast::node { ast::nodes::arr_assignment { parse_child( assign, "array" ),
                                                        parse_child( assign, "index" ),
                                                        parse_child( assign, "value" ), } } ;
    }


    throw std::invalid_argument ( "Invalid element: "s + std::to_string(value.GetType()) );
}


ast::node parse_string ( std::string_view str )
{
    if ( str == "Null" )
        return ast::node { ast::nodes::null {} };

    throw std::invalid_argument ( std::string("Invalid element: ") + std::begin(str) );
}


std::vector<ast::node> parse_object_array ( const rapidjson::Value::ConstArray & array )
{
    auto res = std::vector<ast::node> { };
    res . reserve ( array . Size () );
    for ( const auto & val : array )
        res . emplace_back ( val . IsObject() ? parse_object ( val ) : parse_string(val.GetString()) );
    return res;
}

std::vector<std::string> parse_string_array ( const rapidjson::Value::ConstArray & array )
{
    auto res = std::vector<std::string> { };
    res . reserve ( array . Size () );
    for ( const auto & val : array )
        res . emplace_back ( val . GetString () );
    return res;
}


auto parse_json ( const rapidjson::Document & doc )
{
    auto ret = ast::nodes::top { };
    ret . body = parse_object_array ( doc["Top"] . GetArray () );
    return ast::node { ret };
}


ast::node json_ast_parser::parse_ast ( const std::string & file_path )
{
    auto ifs = std::ifstream { file_path };
    auto doc = get_json_doc ( ifs );

    return parse_json ( doc );
}




