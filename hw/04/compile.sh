#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 compile <program.fml>"
    exit 1
fi

base_dir=$(dirname "$0")

fml_exe="$base_dir"/../../FML/target/release/fml
compiler_exe="$base_dir"/build/compiler
temp_json=tmp.json

./"$fml_exe" parse --format JSON < "$2" > "$temp_json"

if [ "$?" -ne 0 ]; then
    exit 1
fi 

./"$compiler_exe" "$temp_json"
rm "$temp_json"



