

#include "ast/node.hpp"
#include "ast/nodes/block.hpp"
#include "ast/nodes/var.hpp"
#include "ast/nodes/integer.hpp"
#include "ast/nodes/print.hpp"
#include "ast/nodes/var_access.hpp"

#include "ast/visitors/impl/interpreter.hpp"

#include "serialization/program_serializer.hpp"

#include <iostream>
#include <parser/json_ast_parser.hpp>

void print_usage ( int argc, char * argv [] )
{
    std::cout << "Usage: " << argv[0] << " <ast_json_filepath>" << std::endl;
}

int main ( int argc, char * argv [] )
{
    if (argc != 2)
    {
        print_usage(argc, argv);
        return 1;
    }

    std::ios_base::sync_with_stdio ( false );
    //const auto filename = "test.bc";

    auto parser = json_ast_parser {};
    auto top = parser.parse_ast(argv[1]);

    auto interpret = std::make_unique<interpreter>();
    try
    {
        fml::types::code dummy{};
        top . accept ( interpret . get (), dummy, true );


        //auto ofs = std::ofstream { filename, std::ios::binary };
        fml::serialization::serialize_to_bc ( interpret -> get_program(), std::cout );
    }
    catch ( const std::runtime_error & e )
    {
        std::cerr << "Runtime error occurred: " << e . what() << '\n';
    }
}
