//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_STACK_FRAME_HPP
#define INC_02_STACK_FRAME_HPP

#include "ast/node.hpp"
#include "types/types.hpp"

#include <unordered_map>
#include <string>

class stack_frame
{
    std::unordered_map<std::string, fml::types::index_t > _variables;
    std::unordered_map<std::string, ast::node> _functions;
    stack_frame * _parent;

    fml::types::index_t add_local ();

public:
    fml::types::index_t local_count { 0 };

    bool is_function_frame { false };

    explicit stack_frame (stack_frame * parent = nullptr, bool is_function_frame = false );

    [[nodiscard]] std::pair<fml::types::index_t, bool>  get_variable_idx ( const std::string & name ) const;

    [[nodiscard]] const std::unordered_map<std::string, fml::types::index_t > & get_variables ( ) const;

    [[nodiscard]] ast::node get_function ( const std::string & name ) const;

    [[nodiscard]] bool has_function ( const std::string & name ) const;

    fml::types::index_t create_variable ( std::string name );

    void create_function ( std::string name, ast::node val );

    [[nodiscard]]
    bool is_global ( ) const;
};


#endif //INC_02_STACK_FRAME_HPP
