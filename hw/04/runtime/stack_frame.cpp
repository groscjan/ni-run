//
// Created by Jan Groschaft on 3/3/21.
//

#include "stack_frame.hpp"
#include "ast/nodes/null.hpp"

#include <stdexcept>

stack_frame::stack_frame ( stack_frame * parent, bool is_function_frame ) : _parent ( parent ),
        is_function_frame ( is_function_frame )
{
}

std::pair<fml::types::index_t, bool> stack_frame::get_variable_idx ( const std::string & name ) const
{
    if ( auto it = _variables.find(name); it != std::end(_variables) )
        return std::make_pair ( it -> second, is_global() );
    return _parent ? _parent -> get_variable_idx ( name ) : std::make_pair ( fml::types::index_t { 0 }, true );
}


ast::node stack_frame::get_function ( const std::string & name ) const
{
    if ( auto it = _functions.find(name); it != std::end(_functions) )
        return it -> second;
    return _parent ? _parent -> get_function( name ) :
           throw std::runtime_error ( "Lookup failed: "s + name + " doesn't exist in the current scope.");
}




fml::types::index_t  stack_frame::create_variable ( std::string name )
{
    if ( auto it = _variables.find(name); it != std::end(_variables) )
        throw std::runtime_error ( std::string ("Redefinition of " + name ));
    const auto idx = is_global() ? _variables . size() : add_local();
    _variables.emplace(std::move(name), idx );
    return (int)idx;
}

void stack_frame::create_function ( std::string name, ast::node val )
{
    if ( auto it = _functions.find(name); it != std::end(_functions) )
        throw std::runtime_error ( std::string ("Redefinition of " + name ));
    _functions.emplace(std::move(name), std::move(val) );
}

const std::unordered_map<std::string, fml::types::index_t > & stack_frame::get_variables ( ) const
{
    return _variables;
}

bool stack_frame::has_function ( const std::string & name ) const
{
    return _functions . find( name ) != std::end ( _functions );
}

bool stack_frame::is_global ( ) const
{
    return _parent == nullptr;
}

fml::types::index_t stack_frame::add_local ( )
{
    return is_function_frame ? local_count++ : _parent -> add_local();
}





