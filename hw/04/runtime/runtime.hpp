//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_RUNTIME_HPP
#define INC_02_RUNTIME_HPP

#include "stack_frame.hpp"
#include "ast/nodes/obj_ref.hpp"
#include "ast/nodes/object.hpp"

#include <stack>

class runtime
{
    std::stack<stack_frame> _local_frames {};
    stack_frame _global_frame { nullptr, true };

    uint32_t _label_id { 0 };

public:
    stack_frame & push_frame(bool is_function_frame = false);

    stack_frame pop_frame();

    [[nodiscard]] stack_frame & get_current_frame();

    std::string get_label_id ();
};


#endif //INC_02_RUNTIME_HPP
