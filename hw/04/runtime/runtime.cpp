//
// Created by Jan Groschaft on 3/3/21.
//

#include "runtime.hpp"

#include "ast/nodes/obj_def.hpp"


stack_frame & runtime::push_frame ( bool is_function_frame )
{
    auto * prev_frame = is_function_frame ? &_global_frame : _local_frames . empty() ? &_global_frame : &_local_frames . top();
    auto new_frame = stack_frame { prev_frame, is_function_frame };
    _local_frames . push ( std::move ( new_frame ) );
    return _local_frames . top();
}


stack_frame runtime::pop_frame ( )
{
    auto res = std::move ( _local_frames . top() );
    _local_frames . pop();
    return res;
}

stack_frame & runtime::get_current_frame ( )
{
    return _local_frames . empty() ? _global_frame : _local_frames . top();
}

std::string runtime::get_label_id ( )
{
    return std::to_string (_label_id++);
}



