//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_UTILS_HPP
#define INC_02_UTILS_HPP

#include "ast/nodes/boolean.hpp"
#include "ast/nodes/integer.hpp"
#include "ast/nodes/null.hpp"
#include "ast/nodes/arr_ref.hpp"
#include "ast/nodes/obj_ref.hpp"

#include <stdexcept>

class runtime;

namespace ast::utils
{
    template <typename T>
    bool evaluate_truth_value ( const T & val )
    {
        return true;
    }

    template <>
    [[nodiscard]] bool evaluate_truth_value ( const nodes::boolean & val );


    template <>
    [[nodiscard]] bool evaluate_truth_value ( const nodes::null & val );


    template <typename T>
    std::string node_to_string ( const T & val, [[maybe_unused]] const runtime * runtime_info )
    {
        return typeid ( val ) . name ();
    }

    template <>
    std::string node_to_string ( const nodes::integer & val, [[maybe_unused]] const runtime * runtime_info );

    template <>
    std::string node_to_string ( const nodes::boolean & val, [[maybe_unused]] const runtime * runtime_info );

    template <>
    std::string node_to_string ( const nodes::null & val, [[maybe_unused]] const runtime * runtime_info );

    template <>
    std::string node_to_string ( const nodes::arr_ref & val, [[maybe_unused]] const runtime * runtime_info );

    template <>
    std::string node_to_string ( const nodes::obj_ref & val, [[maybe_unused]] const runtime * runtime_info );

    template <typename T>
    [[nodiscard]] bool is_built_in_type ( const T & val )
    {
        return false;
    }

    template <>
    bool is_built_in_type ( const nodes::integer & val );

    template <>
    bool is_built_in_type ( const nodes::boolean & val );

    template <>
    bool is_built_in_type ( const nodes::null & val );
}
#endif //INC_02_UTILS_HPP
