//
// Created by Jan Groschaft on 3/3/21.
//


#include "ast_utils.hpp"
#include "node.hpp"
#include "nodes/method_call.hpp"
#include "runtime/runtime.hpp"

#include <numeric>
#include <string>

using namespace std::literals::string_literals;

namespace ast::utils
{

    template <>
    bool is_built_in_type ( const nodes::integer & val )
    {
        return true;
    }

    template <>
    bool is_built_in_type ( const nodes::boolean & val )
    {
        return true;
    }

    template <>
    bool is_built_in_type ( const nodes::null & val )
    {
        return true;
    }


    template <>
    node dispatch_built_in_type ( const nodes::integer & val, const nodes::method_call & call )
    {
        if ( call . args . size () != 1 )
            throw std::runtime_error ( "There is no method on the integer type that takes "s +
                                       std::to_string ( call . args . size () ) + " arguments." );
        const auto * rhs = call . args[0] . try_get_raw<nodes::integer> ();
        if ( call . name == "==" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { false } } : ast::node {
                    ast::nodes::boolean { val . val == rhs -> val } };
        if ( call . name == "!=" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { true } } : ast::node {
                    ast::nodes::boolean { val . val != rhs -> val } };

        if ( rhs == nullptr )
            throw std::runtime_error ("Invalid operand: integer expected as an operand to "s + call . name );

        if ( call . name == "+" )
            return ast::node { ast::nodes::integer { val . val + rhs -> val } };
        if ( call . name == "-" )
            return ast::node { ast::nodes::integer { val . val - rhs -> val } };
        if ( call . name == "*" )
            return ast::node { ast::nodes::integer { val . val * rhs -> val } };
        if ( call . name == "/" )
            return ast::node { ast::nodes::integer { val . val / rhs -> val } };
        if ( call . name == "%" )
            return ast::node { ast::nodes::integer { val . val % rhs -> val } };
        if ( call . name == "<" )
            return ast::node { ast::nodes::boolean { val . val < rhs -> val } };
        if ( call . name == ">" )
            return ast::node { ast::nodes::boolean { val . val > rhs -> val } };
        if ( call . name == "<=" )
            return ast::node { ast::nodes::boolean { val . val <= rhs -> val } };
        if ( call . name == ">=" )
            return ast::node { ast::nodes::boolean { val . val >= rhs -> val } };
        throw std::runtime_error ( "Unsupported integer method: "s + call . name );
    }

    template <>
    node dispatch_built_in_type ( const nodes::boolean & val, const nodes::method_call & call )
    {
        if ( call . args . size () != 1 )
            throw std::runtime_error ( "There is no method on the boolean type that takes "s +
                                       std::to_string ( call . args . size () ) + " arguments." );
        const auto * rhs = call . args[0] . try_get_raw<ast::nodes::boolean> ();
        if ( call . name == "==" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { false } } : ast::node {
                    ast::nodes::boolean { val . val == rhs -> val } };
        if ( call . name == "!=" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { true } } : ast::node {
                    ast::nodes::boolean { val . val != rhs -> val } };

        if ( rhs == nullptr )
            throw std::runtime_error ( "Unsupported boolean method operand. Boolean expected." );

        if ( call . name == "&" )
            return ast::node { ast::nodes::boolean { val . val && rhs -> val } };
        if ( call . name == "|" )
            return ast::node { ast::nodes::boolean { val . val || rhs -> val } };
        throw std::runtime_error ( "Unsupported boolean method: "s + call . name );
    }

    template <>
    node dispatch_built_in_type ( const nodes::null & val, const nodes::method_call & call )
    {
        if ( call . args . size () != 1 )
            throw std::runtime_error ( "There is no method on the null type that takes "s +
                                       std::to_string ( call . args . size () ) + " arguments." );
        const auto * rhs = call . args[0] . try_get_raw<ast::nodes::null> ();

        if ( call . name == "==" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { false } } : ast::node {
                    ast::nodes::boolean { true } };
        if ( call . name == "!=" )
            return rhs == nullptr ? ast::node { ast::nodes::boolean { true } } : ast::node {
                    ast::nodes::boolean { false } };

        throw std::runtime_error ( "Unsupported null method: "s + call . name );
    }
}
