//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_VISITOR_HPP
#define INC_02_VISITOR_HPP

#include "types/types.hpp"


namespace ast
{
    class node;
}

namespace ast::nodes
{
    struct array;
    struct block;
    struct boolean;
    struct cond;
    struct fn_call;
    struct fn_decl;
    struct integer;
    struct loop;
    struct null;
    struct print;
    struct var_access;
    struct var_assignment;
    struct var;
    struct top;
    struct obj_def;
    struct obj_ref;
    struct field_access;
    struct field_assignment;
    struct method_call;
    struct array;
    struct arr_def;
    struct arr_access;
    struct arr_assignment;
    struct arr_ref;
}

class visitor
{
public:
    virtual ~visitor() = default;

    virtual void visit ( const ast::nodes::block & blk, fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::top & blk , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::boolean & b , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::cond & con , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::fn_call & fn_c , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::fn_decl & fn_d , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::integer & i , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::loop & l , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::null & n , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::print & p , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::var_access & var_access , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::var_assignment & var_assign , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::var & var , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::obj_def & obj , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::obj_ref & ref , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::field_access & field_access , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::field_assignment & field_assignment , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::method_call & m_call , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::arr_def & arr , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::arr_assignment & arr_assign , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::arr_access & arr_access , fml::types::code & code_target, bool emit_result ) = 0;
    virtual void visit ( const ast::nodes::arr_ref & ref , fml::types::code & code_target, bool emit_result ) = 0;

    [[nodiscard]] virtual const fml::program & get_program ( ) const = 0;
};


#endif //INC_02_VISITOR_HPP
