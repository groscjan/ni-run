//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_INTERPRETER_HPP
#define INC_02_INTERPRETER_HPP

#include "runtime/runtime.hpp"
#include "types/types.hpp"
#include <ast/visitors/visitor.hpp>

class interpreter : public visitor
{
    runtime _runtime {};
    fml::program _program {};

    fml::types::index_t compile_fn_def ( const ast::nodes::fn_decl & fn_d, fml::types::code & code_target, bool keep_result, bool is_member );

public:
    void visit ( const ast::nodes::block & blk, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::boolean & b, fml::types::code & code_target, bool keep_result ) override;

    void visit ( const ast::nodes::cond & con, fml::types::code & code_target, bool keep_result ) override;

    void visit ( const ast::nodes::fn_call & fn_c, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::arr_def & arr, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::arr_assignment & arr_assign, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::arr_access & arr_access, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::arr_ref & ref, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::fn_decl & fn_d, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::integer & i, fml::types::code & code_target, bool keep_result ) override;

    void visit ( const ast::nodes::loop & l, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::obj_def & obj, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::obj_ref & ref, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::field_access & field_access, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::field_assignment & field_assignment, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::method_call & m_call, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::null & n, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::print & p, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::var_access & var_access, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::var_assignment & var_assign, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::var & var, fml::types::code & code_target, bool emit_result ) override;

    void visit ( const ast::nodes::top & blk, fml::types::code & code_target, bool emit_result ) override;

    const fml::program & get_program ( ) const override;

};


#endif //INC_02_INTERPRETER_HPP
