//
// Created by Jan Groschaft on 3/3/21.
//

#include "interpreter.hpp"
#include "ast/nodes/block.hpp"
#include "ast/nodes/array.hpp"
#include "ast/nodes/boolean.hpp"
#include "ast/nodes/cond.hpp"
#include "ast/nodes/fn_call.hpp"
#include "ast/nodes/fn_decl.hpp"
#include "ast/nodes/integer.hpp"
#include "ast/nodes/loop.hpp"
#include "ast/nodes/null.hpp"
#include "ast/nodes/print.hpp"
#include "ast/nodes/var.hpp"
#include "ast/nodes/var_access.hpp"
#include "ast/nodes/var_assignment.hpp"
#include "ast/nodes/top.hpp"
#include "ast/nodes/field_access.hpp"
#include "ast/nodes/field_assignment.hpp"
#include "ast/nodes/obj_def.hpp"
#include "ast/nodes/method_call.hpp"
#include "ast/nodes/object.hpp"
#include "ast/nodes/arr_def.hpp"
#include "ast/nodes/arr_assignment.hpp"
#include "ast/nodes/arr_access.hpp"
#include "ast/nodes/arr_ref.hpp"
#include "ast/nodes/array.hpp"
#include "types/types.hpp"

#include <iostream>
#include <algorithm>

using namespace fml::types;


 void  interpreter::visit ( const ast::nodes::top & top, fml::types::code & code_target, bool keep_result )
{
    const auto main_name_idx = _program . register_program_object( string { "main" } );

    auto main_code = code {};

    for ( const auto & statement : top . body )
    {
        const auto is_last = &statement == &top.body.back();
        statement . accept ( this, main_code, is_last );
    }

    const auto code_length = main_code . data . size();
    const auto code_idx = _program . register_code ( main_code );
    const auto locals_length = _runtime . get_current_frame() . local_count;

    const auto main_idx = _program . register_program_object (
            fml::types::method { main_name_idx, code_idx, (uint32_t) code_length, (uint16_t) locals_length, 0  }
            );
    _program . entry_point = main_idx;
}

 void  interpreter::visit ( const ast::nodes::block & blk, fml::types::code & code_target, bool keep_result )
{
    _runtime . push_frame ( false );
    for ( const auto & statement : blk . body )
    {
        const auto is_last = &statement == &blk.body.back();
        statement . accept ( this, code_target, is_last && keep_result );
    }
    _runtime . pop_frame();
}

 void  interpreter::visit ( const ast::nodes::boolean & b, fml::types::code & code_target, bool keep_result )
{
    const auto idx = _program . register_program_object( boolean { b . val } );
    code_target . emit( literal { idx } );
    if ( ! keep_result )
        code_target . emit ( drop {} );
}

void  interpreter::visit ( const ast::nodes::integer & i, fml::types::code & code_target, bool keep_result )
{
    const auto idx = _program . register_program_object( integer { i . val } );
    code_target . emit ( literal { idx } );
    if ( ! keep_result )
        code_target . emit ( drop {} );
}


void  interpreter::visit ( const ast::nodes::null & n, fml::types::code & code_target, bool keep_result )
{
    const auto idx = _program . register_program_object( null { } );
    code_target . emit ( literal { idx } );
    if ( ! keep_result )
        code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::cond & con, fml::types::code & code_target, bool keep_result )
{
    const auto cons_idx = _program . register_program_object ( string { _runtime . get_label_id() } );
    const auto after_idx = _program . register_program_object ( string { _runtime . get_label_id() } );

    con . expr . accept ( this, code_target, true );
    code_target . emit ( branch { cons_idx });
    con . alternative . accept ( this, code_target, keep_result );
    code_target . emit ( jump { after_idx });
    code_target . emit ( label { cons_idx });
    con . consequent . accept ( this, code_target, keep_result );
    code_target . emit ( label { after_idx });
}


void  interpreter::visit ( const ast::nodes::loop & l, fml::types::code & code_target, bool keep_result )
{
    const auto body_label_idx = _program . register_program_object ( string { _runtime . get_label_id() } );
    const auto cond_label_idx = _program . register_program_object ( string { _runtime . get_label_id() } );

    code_target . emit ( jump { cond_label_idx } );
    code_target . emit ( label { body_label_idx } );

    l . body . accept ( this, code_target, false );
    code_target . emit ( label { cond_label_idx } );
    l . cond . accept ( this, code_target, true );
    code_target . emit ( branch { body_label_idx  });

    if ( keep_result )
    {
        const auto idx = _program . register_program_object ( null {} );
        code_target . emit ( literal { idx } );
    }
}

 void  interpreter::visit ( const ast::nodes::fn_call & fn_c, fml::types::code & code_target, bool keep_result )
{
    const auto idx = _program . register_program_object( string { fn_c . name } );
    for ( const auto & arg : fn_c . args )
        arg . accept ( this, code_target, true );
    code_target . emit ( fn_call { idx, (uint8_t)fn_c . args . size() });
    if ( ! keep_result )
        code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::fn_decl & fn_d, fml::types::code & code_target, bool keep_result )
{
    compile_fn_def ( fn_d, code_target, keep_result, false );
}


 void  interpreter::visit ( const ast::nodes::print & p, fml::types::code & code_target, bool keep_result )
{
    const auto idx = _program . register_program_object( string { p . format } );
    for ( const auto & arg : p . args )
        arg . accept ( this, code_target, true );

    code_target . emit ( print { idx, (uint8_t) p . args . size() });
    if ( ! keep_result )
        code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::var_access & var_access, fml::types::code & code_target, bool keep_result )
{
    auto & frame = _runtime . get_current_frame();
    const auto pair = frame . get_variable_idx( var_access . name );

    if ( pair . second  )
    {
        const auto idx = _program . register_program_object( string { var_access . name } );
        code_target . emit ( get_glob { idx } );
    }
    else
    {
        code_target . emit ( get_loc { pair . first } );
    }
}

 void  interpreter::visit ( const ast::nodes::var_assignment & var_assign, fml::types::code & code_target, bool keep_result )
{
    var_assign . expr . accept ( this, code_target, true );

    auto & frame = _runtime . get_current_frame();
    const auto pair = frame . get_variable_idx( var_assign . identifier );

    if ( pair . second  )
    {
        const auto idx = _program . register_program_object( string { var_assign . identifier } );
        code_target . emit ( set_glob { idx } );
    }
    else
    {
        code_target . emit ( set_loc { pair . first } );
    }

    if ( ! keep_result )
        code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::var & var, fml::types::code & code_target, bool keep_result )
{
     var . val . accept ( this, code_target, true );

     auto & frame = _runtime . get_current_frame();
     if ( frame . is_global() ) // we are at global scope
     {
         const auto idx = _program . register_program_object( string { var . name } );
         const auto slot_idx = _program . register_program_object( slot { idx } );
         frame . create_variable ( var . name );
         _program . register_global ( slot_idx );
         code_target . emit ( set_glob { idx } );
     }
     else
     {
        const auto idx = frame . create_variable ( var . name );
        code_target . emit ( set_loc { idx } );
     }

    if ( ! keep_result )
        code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::obj_def & obj, fml::types::code & code_target, bool keep_result )
{
     obj . extends . accept ( this, code_target,true);

     _runtime . push_frame ( false );
     auto slots = std::vector<index_t> {};
     slots . reserve ( obj . members . size() );
     for ( const auto & m : obj . members )
     {
         if ( auto * var = m . try_get_raw<ast::nodes::var> () )
         {
            m . accept (this, code_target, true );
            const auto name_idx = _program . register_program_object ( string { var -> name } );
            const auto slot_idx = _program . register_program_object ( slot { name_idx } );
            slots . push_back ( slot_idx );
         }
         else if ( auto * method = m . try_get_raw<ast::nodes::fn_decl> ())
         {
             const auto method_idx = compile_fn_def ( *method, code_target, keep_result, true );
             slots . push_back ( method_idx );
         }
         else throw std::runtime_error ( "Object can only contain functions and member variables.");
     }
     _runtime . pop_frame();
     const auto class_idx = _program . register_program_object ( class_t { std::move ( slots ) });
     code_target . emit ( obj_def { class_idx } );
     if ( ! keep_result )
         code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::obj_ref & ref, fml::types::code & code_target, bool keep_result )
{
    throw std::runtime_error ( "not implemented");
}

 void  interpreter::visit ( const ast::nodes::field_access & field_access, fml::types::code & code_target, bool keep_result )
{
    field_access . object . accept ( this, code_target, true );
    const auto name_idx = _program . register_program_object ( string { field_access . field} );
    code_target . emit ( get_field { name_idx });
    if (! keep_result )
        code_target . emit ( drop { });
}

 void  interpreter::visit ( const ast::nodes::field_assignment & field_assignment, fml::types::code & code_target, bool keep_result )
{
    field_assignment . object . accept ( this, code_target, true );
    field_assignment . expr . accept ( this, code_target, true );
    const auto name_idx = _program . register_program_object ( string { field_assignment . field} );
    code_target . emit ( set_field { name_idx });
    if (! keep_result )
        code_target . emit ( drop { });
}

 void  interpreter::visit ( const ast::nodes::method_call & method_call, fml::types::code & code_target, bool keep_result )
{
    method_call . object . accept ( this, code_target, true );
    const auto name_idx = _program . register_program_object ( string { method_call . name} );
    for ( auto & arg : method_call . args )
        arg . accept ( this, code_target, true );
    code_target . emit ( m_call { name_idx, (uint8_t) (method_call . args . size () + 1) } );
    if ( !keep_result )
        code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::arr_def & arr, fml::types::code & code_target, bool keep_result )
{
    arr . size . accept ( this, code_target, true );
    arr . value . accept ( this, code_target, true );
    code_target . emit ( arr_def { });
    if ( !keep_result )
        code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::arr_assignment & arr_assign, fml::types::code & code_target, bool keep_result )
{
    arr_assign . array . accept ( this, code_target, true );
    arr_assign . index . accept ( this, code_target, true );
    arr_assign . value  . accept ( this, code_target, true );

    const auto set_idx = _program . register_program_object ( string { "set" } );
    code_target . emit ( m_call { set_idx, 3 });
    if ( !keep_result )
        code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::arr_access & arr_access, fml::types::code & code_target, bool keep_result )
{
    arr_access . array . accept ( this, code_target, true );
    arr_access . index . accept ( this, code_target, true );

    const auto get_idx = _program . register_program_object ( string { "get" } );
    code_target . emit ( m_call { get_idx, 2 });
    if ( !keep_result )
        code_target . emit ( drop {} );
}

 void  interpreter::visit ( const ast::nodes::arr_ref & ref, fml::types::code & code_target, bool keep_result )
{
    throw std::runtime_error ( "not implemented");
}

const fml::program & interpreter::get_program ( ) const
{
    return _program;
}

fml::types::index_t interpreter::compile_fn_def ( const ast::nodes::fn_decl & fn_d, fml::types::code & code_target, bool keep_result, bool is_member )
{
    auto fn_code = code {};

    auto & frame = _runtime . push_frame ( true );

    const auto param_cnt = is_member ? fn_d . params . size() + 1 : fn_d . params . size ();
    if ( is_member )
        frame . create_variable ( "this" );
    for ( const auto & p : fn_d .params )
        frame . create_variable ( p );

    _runtime . push_frame ( false );

    fn_d . body . accept ( this, fn_code, true );
    fn_code . emit ( ret {} );

    _runtime . pop_frame();

    const auto name_idx = _program . register_program_object( string { fn_d . name } );
    const auto code_length = fn_code . data . size();
    const auto code_idx = _program . register_code ( fn_code );
    const auto locals_length = frame . local_count - param_cnt;

    _runtime . pop_frame( );

    const auto fn_idx = _program . register_program_object (
            fml::types::method { name_idx, code_idx, (uint32_t) code_length, (uint16_t) locals_length, (uint8_t)param_cnt  }
    );
    if ( ! is_member )
        _program . register_global ( fn_idx );
    return fn_idx;
}







