//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_ARRAY_HPP
#define INC_02_ARRAY_HPP

#include "../node.hpp"

#include <vector>

namespace ast::nodes
{
    struct array
    {
        std::vector<node> data;
    };
}

#endif //INC_02_ARRAY_HPP
