//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_BLOCK_HPP
#define INC_02_BLOCK_HPP

#include "ast/node.hpp"

#include <vector>

namespace ast::nodes
{
    struct block
    {
        std::vector <node> body;
    };



}
#endif //INC_02_BLOCK_HPP
