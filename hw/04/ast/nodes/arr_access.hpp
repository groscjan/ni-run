//
// Created by Jan Groschaft on 3/10/21.
//

#ifndef INC_02_ARR_ACCESS_HPP
#define INC_02_ARR_ACCESS_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct arr_access
    {
        node array;
        node index;
    };
}

#endif //INC_02_ARR_ACCESS_HPP
