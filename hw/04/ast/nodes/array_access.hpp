//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_ARRAY_ACCESS_HPP
#define INC_02_ARRAY_ACCESS_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct array
    {
        node arr;
        node idx;
    };
}

#endif //INC_02_ARRAY_ACCESS_HPP
