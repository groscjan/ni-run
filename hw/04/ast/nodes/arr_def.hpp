//
// Created by Jan Groschaft on 3/10/21.
//

#ifndef INC_02_ARR_DEF_HPP
#define INC_02_ARR_DEF_HPP


#include "ast/node.hpp"

namespace ast::nodes
{
    struct arr_def
    {
        node size;
        node value;
    };
}

#endif //INC_02_ARR_DEF_HPP
