//
// Created by Jan Groschaft on 3/10/21.
//

#ifndef INC_02_ARR_ASSIGNMENT_HPP
#define INC_02_ARR_ASSIGNMENT_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct arr_assignment
    {
        node array;
        node index;
        node value;
    };
}

#endif //INC_02_ARR_ASSIGNMENT_HPP
