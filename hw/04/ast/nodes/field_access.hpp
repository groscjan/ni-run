//
// Created by Jan Groschaft on 3/5/21.
//

#ifndef INC_02_FIELD_ACCESS_HPP
#define INC_02_FIELD_ACCESS_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct field_access
    {
        node object;
        std::string field;
    };
}

#endif //INC_02_FIELD_ACCESS_HPP
