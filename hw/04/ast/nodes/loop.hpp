//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_LOOP_HPP
#define INC_02_LOOP_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct loop
    {
        node cond;
        node body;
    };
}

#endif //INC_02_LOOP_HPP
