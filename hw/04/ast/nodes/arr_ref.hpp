//
// Created by Jan Groschaft on 3/10/21.
//

#ifndef INC_02_ARR_REF_HPP
#define INC_02_ARR_REF_HPP


namespace ast::nodes
{
    struct arr_ref
    {
        std::size_t heap_idx;
    };
}

#endif //INC_02_ARR_REF_HPP
