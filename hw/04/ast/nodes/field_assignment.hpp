//
// Created by Jan Groschaft on 3/5/21.
//

#ifndef INC_02_FIELD_ASSIGNMENT_HPP
#define INC_02_FIELD_ASSIGNMENT_HPP

#include "ast/node.hpp"

namespace ast::nodes
{
    struct field_assignment
    {
        node object;
        std::string field;
        node expr;
    };
}

#endif //INC_02_FIELD_ASSIGNMENT_HPP
