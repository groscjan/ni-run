//
// Created by Jan Groschaft on 3/5/21.
//

#ifndef INC_02_OBJ_DEF_HPP
#define INC_02_OBJ_DEF_HPP

#include "ast/node.hpp"

#include <vector>

namespace ast::nodes
{
    struct obj_def
    {
        ast::node extends;
        std::vector<ast::node> members;
    };
}


#endif //INC_02_OBJ_DEF_HPP
