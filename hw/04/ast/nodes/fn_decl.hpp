//
// Created by Jan Groschaft on 3/3/21.
//

#ifndef INC_02_FN_DECL_HPP
#define INC_02_FN_DECL_HPP

#include "ast/node.hpp"

#include <string>
#include <vector>

namespace ast::nodes
{
    struct fn_decl
    {
        std::string name;
        std::vector<std::string> params;
        node body;
    };
}

#endif //INC_02_FN_DECL_HPP
