#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 run <program.fml>"
    exit 1
fi

base_dir=$(dirname "$0")

fml_exe="$base_dir"/../../FML/target/release/fml
interpreter_exe="$base_dir"/build/bc
temp_json=tmp.json
temp_bc=tmp.bc

./"$fml_exe" parse --format JSON < "$2" > "$temp_json" && ./"$fml_exe" compile --input-format JSON --output-format bytes "$temp_json" > "$temp_bc"

if [ "$?" -ne 0 ]; then
    exit 1
fi 

./"$interpreter_exe" "$temp_bc"
rm "$temp_bc"
rm "$temp_json"



