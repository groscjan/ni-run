
#include "types/types.hpp"
#include "serialization/serialization.hpp"
#include "runtime/runtime.hpp"
#include "interpreter/interpreter.hpp"

#include <iostream>
#include <chrono>

void print_usage ( int argc, char * argv [] )
{
    std::cout << "Usage: " << argv[0] << " <bc_filepath>" << std::endl;
}

int main ( int argc, char * argv [] )
{
    std::ios_base::sync_with_stdio ( false );

    if (argc != 2)
    {
        print_usage(argc, argv);
        return 1;
    }

    auto bc = fml::serialization::load_bytecode( argv[1] );
    auto prog = fml::serialization::parse_bytecode( bc );
    auto r = fml::runtime { std::move ( prog ), 20, 1000, 1u << 28 };
    auto i = fml::interpreter { std::move ( r ) };

    try
    {

        i . interpret();
    }
    catch ( const std::runtime_error & e )
    {
        std::cerr << "Runtime error occurred: " << e . what() << '\n';
    }
}
