//
// Created by Jan Groschaft on 3/12/21.
//

#ifndef INC_03_TYPES_HPP
#define INC_03_TYPES_HPP

#include "program_objects.hpp"
#include "instructions.hpp"
#include "runtime_objects.hpp"
#include "type_info.hpp"
#include "utils/buffer.hpp"

#include <vector>
#include <unordered_map>

namespace fml
{
    struct label_info
    {
        types::label label;
        std::size_t instruction_idx;
    };

    struct program
    {
        std::vector<types::program_object> constant_pool;
        std::vector<std::string_view> globals;
        std::unordered_map<std::string_view, types::method> functions;
        types::method main_fn;
        std::vector<types::instruction> instructions;
        std::vector<label_info> labels;
    };
}
#endif //INC_03_TYPES_HPP
