//
// Created by Jan Groschaft on 3/16/21.
//

#include "types/type_info.hpp"

#include <variant>
#include <algorithm>
#include <numeric>

using namespace std::literals::string_literals;

namespace fml::types
{
    std::string to_string ( arr_ref ref, const std::vector<types::program_object> & const_pool );

    std::string to_string ( obj_ref ref, const std::vector<types::program_object> & const_pool );


    std::string to_string ( const pointer & ptr, const std::vector<types::program_object> & const_pool )
    {
        return std::visit ( overloaded {
                [] ( const integer & i )
                { return std::to_string ( i . val ); },
                [] ( const boolean & b )
                { return b . val ? "true"s : "false"s; },
                [] ( const null & n )
                { return "null"s; },
                [&const_pool] ( const arr_ref & a )
                { return to_string ( a, const_pool ); },
                [&const_pool] ( const obj_ref & o )
                { return to_string ( o, const_pool ); },
        }, ptr );
    }

    std::string to_string ( arr_ref ref, const std::vector<types::program_object> & const_pool )
    {
        const auto * arr = ref . arr;
        auto res = std::string { '[' };

        res += std::accumulate ( arr -> data (), arr -> data () + arr -> size, std::string { },
                                 [&const_pool] ( std::string context, const pointer & current )
        {
            return context . empty () ? to_string ( current, const_pool ) : std::move ( context ) + ", " + to_string ( current, const_pool );
        } );

        return res + ']';
    }

    std::string to_string ( obj_ref ref, const std::vector<types::program_object> & const_pool )
    {
        const auto * obj = ref . obj;
        const auto c = std::get<class_t> ( const_pool[ref . obj -> class_t_index] );

        auto res = "object(..="s + to_string ( obj -> parent, const_pool );
        for ( auto i = 0ul; i < std::size ( c . fields ); ++i )
            res += ", "s + c . fields[i] . name . data() + '=' + to_string ( obj -> members()[i], const_pool );

        return res + ')';
    }

    bool truth_value ( const pointer & ptr )
    {
        if ( auto * i = std::get_if<boolean> ( &ptr ) )
            return i -> val;
        else if ( std::get_if<null> ( &ptr ) )
            return false;
        else return true;
    }

    pointer prog_obj_to_ptr ( const program_object & po )
    {
        if ( auto * i = std::get_if<integer> ( &po ) )
            return *i;
        else if ( auto * b = std::get_if<boolean> ( &po ) )
            return *b;
        else if ( auto * n = std::get_if<null> ( &po ) )
            return *n;
        else throw std::runtime_error ( "Expected integer, boolean or null." );
    }

}