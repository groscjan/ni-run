//
// Created by Jan Groschaft on 3/12/21.
//

#ifndef INC_03_PROGRAM_OBJECTS_HPP
#define INC_03_PROGRAM_OBJECTS_HPP

#include "instructions.hpp"

#include <cstdint>
#include <vector>
#include <variant>
#include <string_view>


namespace fml::types
{
    struct integer
    {
        int val;
    };

    struct boolean
    {
        bool val;
    };

    struct null
    {
    };

    struct string
    {
        std::string_view str;
    };

    struct slot
    {
        std::string_view name;
    };

    struct method
    {
        std::string_view name;
        instruction_ptr start;
        uint32_t len;
        uint16_t locals;
        uint8_t args;
    };


    struct class_t
    {
        std::vector <slot> fields;
        std::vector <method> methods;
    };

    using program_object = std::variant<integer, boolean, null, string ,slot, method, class_t>;
}

#endif //INC_03_PROGRAM_OBJECTS_HPP
