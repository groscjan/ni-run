//
// Created by Jan Groschaft on 3/14/21.
//

#ifndef INC_03_INSTRUCTIONS_HPP
#define INC_03_INSTRUCTIONS_HPP

#include <variant>
#include <cstdint>

namespace fml
{
    using instruction_ptr = uint32_t;
}

namespace fml::types
{
    using index_t = uint16_t;

    struct literal
    {
        index_t idx;
    };

    struct get_loc
    {
        index_t idx;
    };

    struct set_loc
    {
        index_t idx;
    };


    struct get_glob
    {
        index_t idx;
    };

    struct set_glob
    {
        index_t idx;
    };

    struct fn_call
    {
        index_t idx;
        uint8_t arg_cnt;
    };

    struct ret
    {
    };

    struct label
    {
        index_t idx;
    };

    struct jump
    {
        index_t idx;
    };

    struct branch
    {
        index_t idx;
    };


    struct print
    {
        index_t idx;
        uint8_t arg_cnt;
    };

    struct arr_def
    {
    };

    struct obj_def
    {
        index_t idx;
    };

    struct get_field
    {
        index_t idx;
    };

    struct set_field
    {
        index_t idx;
    };

    struct m_call
    {
        index_t idx;
        uint8_t arg_cnt;
    };

    struct drop
    {
    };

    // special instruction indicating the end of a program
    struct prog_end
    {
    };


    using instruction = std::variant<literal, get_loc, set_loc, get_glob, set_glob,
        fn_call, ret, label, jump, branch, print, arr_def, obj_def, get_field, set_field, m_call, drop, prog_end>;

}


#endif //INC_03_INSTRUCTIONS_HPP
