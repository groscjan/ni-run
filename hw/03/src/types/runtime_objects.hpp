//
// Created by Jan Groschaft on 3/14/21.
//

#ifndef INC_03_RUNTIME_OBJECTS_HPP
#define INC_03_RUNTIME_OBJECTS_HPP

#include "program_objects.hpp"
#include "utils/overloaded.hpp"

#include <variant>
#include <stdexcept>

namespace fml::types
{
    struct object;
    struct array;

    struct obj_ref
    {
        object * obj;
    };

    struct arr_ref
    {
        array * arr;
    };

    using pointer = std::variant<integer, boolean, null, obj_ref, arr_ref>;


    namespace detail
    {
        template <typename T>
        std::byte* bump_behind_object ( T * obj )
        {
            return reinterpret_cast<std::byte*> ( obj ) + sizeof ( T );
        }

        template <typename T>
        const std::byte* bump_behind_object ( const T * obj )
        {
            return reinterpret_cast<const std::byte*> ( obj ) + sizeof ( T );
        }
    }

    struct object
    {
        object ( index_t class_idx, pointer parent ) : class_t_index ( class_idx ), parent( parent )
        {

        }

        index_t class_t_index;
        pointer parent;

        [[nodiscard]] pointer * members ( ) // members are always allocated right behind this object
        {
            return reinterpret_cast<pointer*> (  detail::bump_behind_object ( this ) );
        }

        [[nodiscard]] const pointer * members ( ) const
        {
            return reinterpret_cast<const pointer*> (  detail::bump_behind_object ( this ) );
        }
    };

    struct array
    {
        explicit array ( std::size_t size ) : size ( size )
        {
        }

        std::size_t size;
    public:
        [[nodiscard]] pointer * data ( ) // data is always allocated right behind this object
        {
            return reinterpret_cast<pointer*> (  detail::bump_behind_object ( this ) );
        }

        [[nodiscard]] const pointer * data ( ) const
        {
            return reinterpret_cast<const pointer*> (  detail::bump_behind_object ( this ) );
        }
    };



}

#endif //INC_03_RUNTIME_OBJECTS_HPP
