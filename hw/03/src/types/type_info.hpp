//
// Created by Jan Groschaft on 3/16/21.
//

#ifndef INC_03_TYPE_INFO_HPP
#define INC_03_TYPE_INFO_HPP

#include "runtime_objects.hpp"

namespace fml
{
    struct runtime;
    namespace types
    {
        std::string to_string ( const types::pointer & ptr, const std::vector<types::program_object> & const_pool );

        bool truth_value ( const types::pointer & ptr );

        pointer prog_obj_to_ptr ( const program_object & po );

    }
}

#endif //INC_03_TYPE_INFO_HPP
