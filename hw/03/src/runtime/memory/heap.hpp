//
// Created by Jan Groschaft on 3/15/21.
//

#ifndef INC_03_HEAP_HPP
#define INC_03_HEAP_HPP

#include "allocators/linear_allocator.hpp"
#include "types/types.hpp"

#include <span>

namespace fml::memory
{
    using namespace types;

    class heap
    {
        linear_allocator _alloc;
    public:
        explicit heap ( std::size_t size );

        pointer allocate_array ( std::size_t size, pointer val );

        pointer allocate_object ( const class_t & c, index_t class_t_index, pointer parent, std::span<pointer> members );

        [[nodiscard]]
        std::size_t capacity () const noexcept;

        [[nodiscard]]
        std::size_t size () const noexcept;
    };
}


#endif //INC_03_HEAP_HPP
