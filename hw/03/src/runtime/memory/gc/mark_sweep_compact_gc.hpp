//
// Created by Jan Groschaft on 3/21/21.
//

#ifndef INC_03_MARK_SWEEP_COMPACT_GC_HPP
#define INC_03_MARK_SWEEP_COMPACT_GC_HPP

#include "runtime/stack/frame_stack.hpp"
#include "runtime/memory/heap.hpp"

namespace fml::memory
{
    class mark_sweep_compact_gc
    {
    public:

        void collect ( const operand_stack & op_stack, const frame_stack & f_stack, heap & h );

    };
}

#endif //INC_03_MARK_SWEEP_COMPACT_GC_HPP
