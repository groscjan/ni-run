//
// Created by Jan Groschaft on 3/21/21.
//

#include "mark_sweep_compact_gc.hpp"

#include <algorithm>
#include <ranges>

using namespace fml;

void fml::memory::mark_sweep_compact_gc::collect ( const fml::operand_stack & op_stack, const fml::frame_stack & f_stack, fml::memory::heap & h )
{
    const auto operands = op_stack . peek_n ( op_stack . size());

    for ( const auto & elem : operands )
    {
        if ( const auto * obj_ptr = std::get_if<types::obj_ref> ( &elem ) )
        {
            //obj_ptr -> obj ->
        }
        const auto * ptr =  std::get_if<types::obj_ref> ( &elem );
    }


    auto mem_resource = linear_allocator { h . capacity() * 2 };


}
