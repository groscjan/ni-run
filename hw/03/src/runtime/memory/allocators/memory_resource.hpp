//
// Created by Jan Groschaft on 10/8/20.
//

#ifndef INC_03_MEMORY_RESOURCE_HPP
#define INC_03_MEMORY_RESOURCE_HPP

#include <cstdint>
#include <concepts>

namespace fml::memory
{
    template <typename T>
    concept memory_resource = requires ( T a, void* ptr, std::size_t n, std::size_t alignment)
    {
        {a . allocate(n, alignment)} -> std::convertible_to<void*>;
        {a . deallocate(ptr, n)};
    };
}


#endif //INC_03_MEMORY_RESOURCE_HPP
