//
// Created by Jan Groschaft on 4.7.19.
//

#ifndef INC_03_LINEAR_ALLOCATOR_HPP
#define INC_03_LINEAR_ALLOCATOR_HPP

#include "allocator.hpp"
#include <cstddef>
#include <cassert>

namespace fml::memory
{
    class linear_allocator
    {
        std::unique_ptr<std::byte[]> _data;
        std::byte * _top;
        std::size_t _size;

        [[nodiscard]] static inline std::size_t offset_to_aligned ( void * ptr, std::size_t alignment) noexcept;

    public:
        explicit linear_allocator ( std::size_t size );

        [[nodiscard]] void * allocate ( std::size_t n, std::size_t alignment = alignof(std::max_align_t) ) noexcept;

        void deallocate ( void * ptr, std::size_t n ) noexcept;

        [[nodiscard]] bool owns ( void * ptr, [[maybe_unused]] std::size_t n ) const noexcept;

        [[nodiscard]] std::size_t capacity ( ) const noexcept;

        [[nodiscard]] std::size_t size ( ) const noexcept;

        void deallocate_all ( ) noexcept;
    };


}

#endif //INC_03_LINEAR_ALLOCATOR_HPP
