//
// Created by Jan Groschaft on 3/15/21.
//

#include "runtime/memory/allocators/linear_allocator.hpp"

#include <cstdint>
#include <iterator>


fml::memory::linear_allocator::linear_allocator ( std::size_t size ): _data ( std::make_unique<std::byte[]>( size ) ), _top ( _data . get() ),
    _size ( size )
{

}

void * fml::memory::linear_allocator::allocate ( std::size_t n, std::size_t alignment ) noexcept
{
    if ( !n ) return nullptr;
    auto offset = offset_to_aligned( _top, alignment );
    auto actual_size = offset + n;
    if ( _top + actual_size > _data . get() + _size )
        return nullptr;
    auto ret = _top + offset;
    _top += actual_size;
    return ret;
}

void fml::memory::linear_allocator::deallocate ( void * ptr, std::size_t n ) noexcept
{
    if ( static_cast<std::byte*> ( ptr ) + n == _top )
        _top = static_cast<std::byte*> ( ptr );
}

bool fml::memory::linear_allocator::owns ( void * ptr, std::size_t n ) const noexcept
{
    return ptr >= _data . get() && ptr < _data . get() + _size;
}

std::size_t fml::memory::linear_allocator::capacity ( ) const noexcept
{
    return _size;
}

void fml::memory::linear_allocator::deallocate_all ( ) noexcept
{
    _top = _data . get();
}

std::size_t fml::memory::linear_allocator::offset_to_aligned ( void * ptr, std::size_t alignment ) noexcept
{
    assert ( alignment && !( alignment & ( alignment - 1 ) ) &&
             "Alignment must be a power of two." );
    return reinterpret_cast<uintptr_t> ( ptr ) & ( alignment - 1 );
}

std::size_t fml::memory::linear_allocator::size ( ) const noexcept
{
    return std::distance ( _data . get(), _top );
}
