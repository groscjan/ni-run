//
// Created by Jan Groschaft on 4.7.19.
//

#ifndef INC_03_ALLOCATOR_HPP
#define INC_03_ALLOCATOR_HPP

#include "memory_resource.hpp"

#include <cstdint>
#include <memory>

namespace fml::memory
{
    template <typename T, memory_resource mem_resource>
    class allocator
    {
        template<typename, memory_resource> friend class allocator;
        mem_resource * _mem_resource;

    public:
        using value_type = T;

        explicit allocator( mem_resource * resource ) : _mem_resource ( resource )
        {
        }

        template <typename U, memory_resource mem_res>
        explicit allocator ( const allocator<U, mem_res> & other ) : _mem_resource ( other._mem_resource )
        {

        }

        template <class U>
        struct rebind
        {
            typedef allocator<U, mem_resource> other;
        };


        T * allocate ( std::size_t n )
        {
            auto ptr = _mem_resource -> allocate ( n * sizeof ( T ) );
            return static_cast<T*>(ptr);
        }

        void deallocate ( T * ptr, std::size_t n )
        {
            _mem_resource -> deallocate ( ptr, n * sizeof(T) );
        }

    };

}


#endif //INC_03_ALLOCATOR_HPP
