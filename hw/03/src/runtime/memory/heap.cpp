//
// Created by Jan Groschaft on 3/15/21.
//

#include "runtime/memory/heap.hpp"

#include <stdexcept>
#include <iostream>

using namespace fml::types;

fml::memory::heap::heap ( std::size_t size ) : _alloc( size )
{
}

pointer fml::memory::heap::allocate_object ( const fml::types::class_t & c, index_t class_t_index, pointer parent, std::span<pointer> members )
{
    auto * mem = _alloc . allocate( sizeof (object) + std::size ( members ) * sizeof ( pointer ), alignof (object) );
    if ( ! mem )
        throw std::runtime_error ( "Out of memory: heap full.");

    auto * obj = new ( mem ) object ( class_t_index, parent );
    std::uninitialized_copy (std::begin ( members ), std::end( members ),  obj -> members() );

    return obj_ref { obj };
}

pointer fml::memory::heap::allocate_array ( std::size_t size, pointer val )
{
    auto * mem = _alloc . allocate( sizeof ( array ) + size * sizeof(pointer), alignof (array) );
    if ( ! mem )
        throw std::runtime_error ( "Out of memory: heap full.");

    auto * arr = new ( mem ) array ( size );
    std::uninitialized_fill_n ( arr -> data(), size, val );

    return arr_ref { arr };
}

std::size_t fml::memory::heap::capacity ( ) const noexcept
{
    return _alloc . capacity();
}

std::size_t fml::memory::heap::size ( ) const noexcept
{
    return _alloc . size ();
}
