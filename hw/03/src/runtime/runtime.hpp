//
// Created by Jan Groschaft on 3/12/21.
//

#ifndef INC_03_RUNTIME_HPP
#define INC_03_RUNTIME_HPP

#include "runtime/stack/frame_stack.hpp"
#include "types/types.hpp"
#include "memory/heap.hpp"

namespace fml
{
    struct runtime
    {
        runtime ( program p, size_t frame_stack_size, size_t operand_stack_size, size_t heap_size );

        std::vector<types::program_object> constant_pool;
        frame_stack frames;
        stack<types::pointer> operand_stack;
        memory::heap heap;
        instruction_ptr ip;
        std::vector<types::instruction> instructions;
        std::unordered_map<std::string_view, instruction_ptr> labels {};
    };
}

#endif //INC_03_RUNTIME_HPP
