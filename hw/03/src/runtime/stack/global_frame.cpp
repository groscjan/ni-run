//
// Created by Jan Groschaft on 3/14/21.
//

#include "runtime/stack/global_frame.hpp"

#include <stdexcept>

using namespace std::literals::string_literals;


fml::global_frame::global_frame ( const std::vector<std::string_view> & globals, std::unordered_map<std::string_view, types::method> global_funcs )
        : _global_functions(std::move ( global_funcs ))
{
    _globals . reserve( std::size ( globals ) );
    for ( const auto & global : globals )
        _globals . insert( {global, types::pointer { types::null {} } } );
}

fml::types::method fml::global_frame::get_fn ( std::string_view name ) const
{
    if ( auto it = _global_functions . find( name ); it != std::end(_global_functions) )
        return it -> second;
    throw std::runtime_error ( "No function named "s + name . data() );
}

fml::types::pointer fml::global_frame::get_global ( std::string_view name ) const
{
    if ( auto it = _globals . find( name ); it != std::end(_globals) )
        return it -> second;
    throw std::runtime_error ( "No global named "s + name . data() );
}

void fml::global_frame::set_global ( std::string_view name, fml::types::pointer val )
{
    if ( auto it = _globals . find( name ); it != std::end(_globals) )
    {
        it -> second = val;
        return;
    }
    throw std::runtime_error ( "No global named "s + name . data() );
}


