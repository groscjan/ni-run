//
// Created by Jan Groschaft on 3/14/21.
//

#ifndef INC_03_FRAME_STACK_HPP
#define INC_03_FRAME_STACK_HPP

#include "types/types.hpp"
#include "stack.hpp"
#include "local_frame.hpp"
#include "global_frame.hpp"

namespace fml
{
    class frame_stack
    {
        global_frame _global;
        stack<local_frame> _stack;

    public:
        explicit frame_stack ( global_frame global, std::size_t default_stack_size );

        void push_frame ( std::span<types::pointer> args, uint16_t local_cnt, instruction_ptr ret_addr );

        void push_object_frame ( types::pointer obj_ref, std::span<types::pointer> args, uint16_t local_cnt, instruction_ptr ret_addr );

        instruction_ptr pop_frame ();

        [[nodiscard]] types::pointer get_local ( types::index_t index ) const;

        void set_local ( types::index_t index, types::pointer val );

        [[nodiscard]] types::pointer get_global ( std::string_view name ) const;

        void set_global ( std::string_view name, types::pointer val );

        [[nodiscard]] types::method get_fn ( std::string_view name ) const;
    };
}


#endif //INC_03_FRAME_STACK_HPP
