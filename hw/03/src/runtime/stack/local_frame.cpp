//
// Created by Jan Groschaft on 3/14/21.
//

#include "runtime/stack/local_frame.hpp"
#include <cassert>
#include <ranges>

fml::local_frame::local_frame ( std::span<types::pointer> args, uint16_t local_cnt, instruction_ptr ret_addr ) :
    ret_addr ( ret_addr )
{
    _locals . reserve( std::size ( args ) + local_cnt );
    std::ranges::copy ( args, std::back_inserter( _locals ) );
    std::fill_n ( std::back_inserter( _locals ), local_cnt, types::null {} );
}

fml::local_frame::local_frame ( fml::types::pointer obj_ref, std::span<types::pointer> args, uint16_t local_cnt, fml::instruction_ptr ret_addr ) :
    ret_addr ( ret_addr )
{
    _locals . reserve( std::size ( args ) + local_cnt + 1 );
    _locals . push_back ( obj_ref );
    std::ranges::copy ( args, std::back_inserter( _locals ) );
    std::fill_n ( std::back_inserter( _locals ), local_cnt, types::null {} );
}

const fml::types::pointer & fml::local_frame::get_local ( fml::types::index_t index ) const
{
    assert( index < std::size (_locals) && "Index out of range." );
    return _locals[index];
}

void fml::local_frame::set_local ( fml::types::index_t index, fml::types::pointer val )
{
    assert( index < std::size (_locals) && "Index out of range." );
    _locals[index] = val;
}


