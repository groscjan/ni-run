//
// Created by Jan Groschaft on 3/14/21.
//

#ifndef INC_03_STACK_HPP
#define INC_03_STACK_HPP

#include "types/types.hpp"

#include <cassert>
#include <span>

namespace fml
{
    template <typename T>
    class stack
    {
        std::vector<T> _stack {};
    public:
        explicit stack ( std::size_t default_size )
        {
            _stack . reserve( default_size );
        }

        void push ( T val )
        {
            _stack . push_back( std::move ( val ) );
        }

        template <typename ... Ts>
        void emplace ( Ts&& ... args )
        {
            _stack . emplace_back ( std::forward( args ) ... );
        }

        T pop () noexcept
        {
            assert(! std::empty(_stack) );
            auto ret = _stack . back();
            _stack . pop_back();
            return ret;
        }

        void pop_n ( std::size_t n ) noexcept
        {
            assert( std::size ( _stack ) >= n );
            _stack . resize( std::size ( _stack ) - n );
        }

        std::span<T> peek_n ( std::size_t n ) noexcept
        {
            assert ( std::size (_stack) >= n );
            return { std::end ( _stack ) - n, n };
        }

        std::span<const T> peek_n ( std::size_t n ) const noexcept
        {
            assert ( std::size (_stack) >= n );
            return { std::end ( _stack ) - n, n };
        }

        [[nodiscard]] const T& peek () const
        {
            assert(! std::empty(_stack) );
            return _stack . back();
        }

        [[nodiscard]] T& peek ()
        {
            assert(! std::empty(_stack) );
            return _stack . back();
        }

        [[nodiscard]] std::size_t size ( ) const noexcept
        {
            return std::size ( _stack );
        }

        void shrink_to_fit ()
        {
            if ( std::size(_stack) * 2 < _stack . capacity() )
                _stack . resize( std::size(_stack) * 2 );
        }
    };

    using operand_stack = stack<types::pointer>;
}

#endif //INC_03_STACK_HPP
