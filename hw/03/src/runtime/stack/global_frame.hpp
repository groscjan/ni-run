//
// Created by Jan Groschaft on 3/14/21.
//

#ifndef INC_03_GLOBAL_FRAME_HPP
#define INC_03_GLOBAL_FRAME_HPP

#include "types/types.hpp"

#include <string_view>
#include <unordered_map>

namespace fml
{
    class global_frame
    {
        std::unordered_map<std::string_view, types::pointer> _globals {};

        std::unordered_map<std::string_view, types::method> _global_functions;

    public:
        explicit global_frame ( const std::vector<std::string_view> & globals, std::unordered_map<std::string_view, types::method> global_funcs );

        types::method get_fn ( std::string_view name ) const;

        types::pointer get_global ( std::string_view name ) const;

        void set_global ( std::string_view name, types::pointer val );

    };
}



#endif //INC_03_GLOBAL_FRAME_HPP
