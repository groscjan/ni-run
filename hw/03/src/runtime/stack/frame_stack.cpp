//
// Created by Jan Groschaft on 3/14/21.
//

#include "runtime/stack/frame_stack.hpp"


fml::frame_stack::frame_stack ( fml::global_frame global, std::size_t default_stack_size ) : _global( std::move ( global ) ),
    _stack( default_stack_size )
{

}

void fml::frame_stack::push_frame ( std::span<types::pointer> args, uint16_t local_cnt, instruction_ptr ret_addr )
{
    _stack . push( local_frame { args, local_cnt, ret_addr } );
}

void fml::frame_stack::push_object_frame ( fml::types::pointer obj_ref, std::span<types::pointer> args, uint16_t local_cnt, fml::instruction_ptr ret_addr )
{
    _stack . push( local_frame { obj_ref, args, local_cnt, ret_addr } );

}


fml::instruction_ptr fml::frame_stack::pop_frame ( )
{
    auto ret = _stack . peek() . ret_addr;
    _stack . pop();
    return ret;
}

fml::types::pointer fml::frame_stack::get_local ( types::index_t index ) const
{
    return _stack . peek() . get_local( index );
}

void fml::frame_stack::set_local ( types::index_t index, fml::types::pointer val )
{
    _stack . peek() . set_local ( index, val );
}

fml::types::pointer fml::frame_stack::get_global ( std::string_view name ) const
{
    return _global . get_global( name );
}

void fml::frame_stack::set_global ( std::string_view name, fml::types::pointer val )
{
    _global . set_global( name, val );
}

fml::types::method fml::frame_stack::get_fn ( std::string_view name ) const
{
    return _global . get_fn ( name );
}



