//
// Created by Jan Groschaft on 3/14/21.
//

#ifndef INC_03_LOCAL_FRAME_HPP
#define INC_03_LOCAL_FRAME_HPP

#include "types/types.hpp"

#include <span>
#include <vector>

namespace fml
{
    class local_frame
    {
        std::vector<types::pointer> _locals;
    public:
        const instruction_ptr ret_addr;

        local_frame ( std::span<types::pointer> args, uint16_t local_cnt, instruction_ptr ret_addr );

        local_frame ( types::pointer obj_ref, std::span<types::pointer> args, uint16_t local_cnt, instruction_ptr ret_addr );

        [[nodiscard]] const types::pointer& get_local ( types::index_t index ) const;

        void set_local ( types::index_t index, types::pointer val );
    };
}

#endif //INC_03_LOCAL_FRAME_HPP
