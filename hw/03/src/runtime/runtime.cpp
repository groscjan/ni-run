//
// Created by Jan Groschaft on 3/12/21.
//

#include "runtime/runtime.hpp"
#include <chrono>
#include <iostream>

fml::runtime::runtime ( fml::program p, std::size_t frame_stack_size, std::size_t operand_stack_size, size_t heap_size ) : constant_pool( std::move (p . constant_pool ) ),
    frames( global_frame { p . globals, std::move ( p . functions ) }, frame_stack_size ), operand_stack( operand_stack_size ), heap ( heap_size ),
    ip ( p . main_fn . start ), instructions( std::move ( p . instructions ) )
{
    instructions . emplace_back( types::prog_end {} );
    frames . push_frame( {}, p . main_fn . locals, std::size ( instructions ) - 1 );
    for ( const auto & label : p . labels )
    {
        const auto label_name = std::get<types::string> ( constant_pool[label . label . idx ] );
        labels [label_name . str] = label . instruction_idx;
    }
}
