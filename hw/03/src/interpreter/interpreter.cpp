//
// Created by Jan Groschaft on 3/12/21.
//

#include "interpreter/interpreter.hpp"
#include "types/types.hpp"
#include "utils/output.hpp"
#include "interpreter/instruction_dispatcher.hpp"
#include "interpreter/method_dispatcher.hpp"

#include <variant>
#include <algorithm>
#include <ranges>

namespace fml
{
    using namespace types;
    using namespace std::literals::string_literals;


}

fml::interpreter::interpreter ( fml::runtime r ) : _runtime( std::move ( r ) )
{
}





void fml::interpreter::interpret ( )
{
    auto & ip = _runtime . ip;
    auto v = instruction_dispatcher ( &_runtime, &_should_run );

    for  ( ;; )
    {
        auto i = _runtime . instructions[ip];

        std::visit( v, i );

        [[unlikely]]
        if ( ! _should_run )
            break;
    }

}
