//
// Created by Jan Groschaft on 3/12/21.
//

#ifndef INC_03_INTERPRETER_HPP
#define INC_03_INTERPRETER_HPP

#include "runtime/runtime.hpp"
#include "utils/buffer.hpp"

namespace fml
{
    class interpreter
    {
        runtime _runtime;
        bool _should_run { true };
    public:
        explicit interpreter( runtime r );

        void interpret ( );
    };

}


#endif //INC_03_INTERPRETER_HPP
