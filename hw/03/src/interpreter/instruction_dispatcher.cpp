//
// Created by Jan Groschaft on 3/19/21.
//

#include "interpreter/method_dispatcher.hpp"
#include "utils/output.hpp"
#include "types/types.hpp"
#include "interpreter/instruction_dispatcher.hpp"

#include <ranges>
#include <algorithm>
#include <variant>

using namespace fml;
using namespace std::string_literals;

template <typename T>
auto find_idx ( const std::vector<T> & fields, std::string_view name )
{
    auto it = std::ranges::find_if( fields, [name] ( const auto & item ) { return item.name == name; } );
    if ( it == std::end ( fields ) )
        throw std::runtime_error ( "Invalid field name: "s + name . data() );

    return std::distance( std::begin ( fields ), it );
}

instruction_dispatcher::instruction_dispatcher ( fml::runtime * r, bool * should_run ) : _r ( r ), _should_run ( should_run ) { }

void instruction_dispatcher::operator () ( fml::types::literal l )
{
    auto ptr = prog_obj_to_ptr( _r -> constant_pool[l.idx] );
    _r -> operand_stack . push( ptr );
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::get_loc l )
{
    auto ptr = _r -> frames . get_local( l . idx );
    _r -> operand_stack . push( ptr );
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::set_loc l )
{
    auto ptr = _r -> operand_stack . peek();
    _r -> frames . set_local( l . idx, ptr );
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::get_glob g )
{
    auto name = std::get<fml::types::string> ( _r -> constant_pool[g . idx] );
    auto global = _r -> frames . get_global( name . str );
    _r -> operand_stack . push( global );
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::set_glob g )
{
    auto name = std::get<fml::types::string> ( _r -> constant_pool[g . idx] );
    auto ptr = _r -> operand_stack . peek();
    _r -> frames . set_global( name . str, ptr );
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::fn_call f )
{
    auto name = std::get<fml::types::string> ( _r -> constant_pool[f . idx] );
    auto fn = _r -> frames . get_fn(name . str);

    auto args = _r -> operand_stack . peek_n( f . arg_cnt );
    _r -> frames . push_frame( args, fn . locals, _r -> ip + 1 );
    _r -> operand_stack . pop_n( f . arg_cnt );
    _r -> ip = fn . start;
}

void instruction_dispatcher::operator () ( fml::types::ret r )
{
    auto ret_addr = _r -> frames . pop_frame();
    _r -> ip = ret_addr;
}

void instruction_dispatcher::operator () ( fml::types::label l )
{
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::jump j )
{
    auto label_name = std::get<fml::types::string> ( _r -> constant_pool[j . idx] );
    if ( auto it = _r -> labels . find( label_name.str ); it != std::end ( _r -> labels ) )
    {
        auto target_instr = it -> second;
        _r -> ip = target_instr;
    }
    else throw std::runtime_error ( "Unknown label: "s + label_name . str . data() );
}

void instruction_dispatcher::operator () ( fml::types::branch b )
{
    auto ptr = _r -> operand_stack . pop ();
    if ( truth_value(ptr) )
        this -> operator() ( fml::types::jump{ b . idx } );
    else
        _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::print p )
{
    auto fmt_str = std::get<fml::types::string> ( _r -> constant_pool[p . idx] );
    auto args = _r -> operand_stack . peek_n( p . arg_cnt );
    print_to_stdout(fmt_str . str, args, _r -> constant_pool );
    _r -> operand_stack . pop_n( p . arg_cnt );
    _r -> operand_stack . push( fml::types::null{} );
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::arr_def a )
{
    auto val = _r -> operand_stack . pop ();
    auto size = std::get<types::integer> ( _r -> operand_stack . pop () ) . val;
    if ( size < 0 )
        throw std::runtime_error ( "Invalid array size: "s + std::to_string ( size ) );

    auto ref = _r -> heap . allocate_array( size, val );
    _r -> operand_stack . push ( ref );
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::obj_def o )
{
    auto c = std::get<fml::types::class_t> ( _r -> constant_pool[o . idx] );
    auto span = _r -> operand_stack . peek_n ( std::size ( c . fields ) + 1 ); //peek members + parent
    auto parent = span . front ();
    auto members = span . last ( std::size ( c . fields ) );

    auto ref = _r -> heap . allocate_object( c, o . idx, parent, members );

    _r -> operand_stack . pop_n( std::size ( c . fields ) + 1 ); //pop members + parent
    _r -> operand_stack . push ( ref );
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::get_field f )
{
    auto field_name = std::get<fml::types::string> ( _r -> constant_pool[f . idx] );
    auto ref = std::get<fml::types::obj_ref> ( _r -> operand_stack . pop() );
    auto c = std::get<fml::types::class_t> ( _r -> constant_pool[ref . obj -> class_t_index] );

    auto idx = find_idx ( c . fields, field_name . str );
    _r -> operand_stack . push ( ref . obj -> members()[idx] );
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::set_field f )
{
    auto field_name = std::get<fml::types::string> ( _r -> constant_pool[f . idx] );
    auto val = _r -> operand_stack . pop();
    auto ref = std::get<fml::types::obj_ref> ( _r -> operand_stack . pop() );
    auto c = std::get<fml::types::class_t> ( _r -> constant_pool[ref . obj -> class_t_index] );

    auto idx = find_idx ( c . fields, field_name . str );

    ref . obj -> members()[idx] = val;
    _r -> operand_stack . push(val);
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::m_call m )
{
    auto method_name = std::get<fml::types::string> ( _r -> constant_pool[m . idx] );
    auto span = _r -> operand_stack . peek_n( m . arg_cnt );
    auto receiver = span . front();
    auto args = span . last(m.arg_cnt - 1);

    auto dispatch = method_dispatcher { method_name . str, args, _r };

    for ( ;; )
    {
        auto [method_found, result] = std::visit ( dispatch, receiver );
        if ( method_found )
        {
            _r -> operand_stack . pop_n ( m . arg_cnt );
            if ( result . has_value() )
                _r -> operand_stack . push ( result . value() );
            return;
        }

        // here the receiver must be obj_ref, coz for built-in types we would have thrown an exception
        receiver = std::get<fml::types::obj_ref> ( receiver ) . obj -> parent;
    }
}

void instruction_dispatcher::operator () ( fml::types::drop d )
{
    _r -> operand_stack . pop();
    _r -> ip++;
}

void instruction_dispatcher::operator () ( fml::types::prog_end end ) const
{
    *_should_run = false;
}
