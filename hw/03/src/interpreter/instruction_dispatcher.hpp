//
// Created by Jan Groschaft on 3/19/21.
//

#ifndef INC_03_INSTRUCTION_DISPATCHER_HPP
#define INC_03_INSTRUCTION_DISPATCHER_HPP


#include "types/types.hpp"
#include "runtime/runtime.hpp"

namespace fml
{
    class instruction_dispatcher
    {
        runtime * const _r;
        bool * const _should_run;
    public:
        explicit instruction_dispatcher ( runtime * r, bool * should_run );

        void operator () ( types::literal l);
        void operator () ( types::get_loc l);
        void operator () ( types::set_loc l);
        void operator () ( types::get_glob g);
        void operator () ( types::set_glob g);
        void operator () ( types::fn_call f);
        void operator () ( types::ret r);
        void operator () ( types::label l);
        void operator () ( types::jump j);
        void operator () ( types::branch b);
        void operator () ( types::print p);
        void operator () ( types::arr_def a);
        void operator () ( types::obj_def o);
        void operator () ( types::get_field f);
        void operator () ( types::set_field f);
        void operator () ( types::m_call m);
        void operator () ( types::drop d);
        void operator () ( types::prog_end end) const;
    };
}

#endif //INC_03_INSTRUCTION_DISPATCHER_HPP
