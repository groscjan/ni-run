//
// Created by Jan Groschaft on 3/19/21.
//

#include "interpreter/method_dispatcher.hpp"

#include <ranges>
#include <algorithm>
#include <variant>
#include <concepts>

using namespace std::string_literals;
using namespace fml::types;

void fml::method_dispatcher::check_arg_cnt ( std::size_t expected_cnt )
{
    if ( expected_cnt != std::size ( _args ) )
    {
        throw std::runtime_error ( "Invalid number of arguments for method "s + _method_name . data () +
                                   ". Expected " + std::to_string ( expected_cnt ) + ", provided " +
                                   std::to_string ( std::size ( _args ) ) );
    }
}

[[ noreturn ]]
void invalid_method_exception ( std::string_view type_name, std::string_view method_name )
{
    throw std::runtime_error ( "Unsupported method call for type "s + type_name . data () + ": " + method_name .data () );
}



fml::method_dispatcher::method_dispatcher ( std::string_view method_name, std::span<fml::types::pointer> args, fml::runtime * const r ) : _method_name ( method_name ),
                                                                                                                      _args( args ), _r ( r )
{ }

fml::method_dispatch_result fml::method_dispatcher::operator () ( fml::types::integer i )
{
    check_arg_cnt ( 1 );
    auto arg = _args . front();

    _r -> ip ++;
    auto * rhs = std::get_if<integer> ( &arg );
    if ( _method_name == "==" || _method_name == "eq" )
        return { true, rhs ? boolean { rhs -> val == i . val } : boolean { false } };
    if ( _method_name == "!=" || _method_name == "neq" )
        return { true, rhs ? boolean { rhs -> val != i . val } : boolean { true } };
    if ( ! rhs )
        throw std::runtime_error ( "Integer expected as the operand to "s + _method_name . data () );
    if ( _method_name == "+" || _method_name == "add" )
        return { true, integer { i . val + rhs -> val } };
    if ( _method_name == "-" || _method_name == "sub" )
        return { true, integer { i . val - rhs -> val } };
    if ( _method_name == "*" || _method_name == "mul" )
        return { true, integer { i . val * rhs -> val } };
    if ( _method_name == "/" || _method_name == "div" )
        return { true, integer { i . val / rhs -> val } };
    if ( _method_name == "%" || _method_name == "mod" )
        return { true, integer { i . val % rhs -> val } };
    if ( _method_name == "<=" || _method_name == "le" )
        return { true, boolean { i . val <= rhs -> val } };
    if ( _method_name == "<" || _method_name == "lt" )
        return { true, boolean { i . val < rhs -> val } };
    if ( _method_name == ">=" || _method_name == "ge" )
        return { true, boolean { i . val >= rhs -> val } };
    if ( _method_name == ">" || _method_name == "gt" )
        return { true, boolean { i . val > rhs -> val } };
    invalid_method_exception ( "integer", _method_name );
}

fml::method_dispatch_result fml::method_dispatcher::operator () ( fml::types::null n )
{
    check_arg_cnt ( 1 );
    _r -> ip ++;
    auto arg = _args . front();
    if ( _method_name == "==" || _method_name == "eq" )
        return { true, std::get_if<null> ( &arg ) ? boolean { true } : boolean { false } };
    else if ( _method_name == "!=" || _method_name == "neq")
        return { true, std::get_if<null> ( &arg ) ? boolean { false } : boolean { true } };
    invalid_method_exception ( "null", _method_name );
}

fml::method_dispatch_result fml::method_dispatcher::operator () ( fml::types::boolean b )
{
    check_arg_cnt ( 1 );
    auto arg = _args . front();

    _r -> ip ++;
    auto * rhs = std::get_if<boolean> ( &arg );
    if ( _method_name == "==" || _method_name == "eq" )
        return { true, rhs ? boolean { rhs -> val == b . val } : boolean { false } };
    if ( _method_name == "!=" || _method_name == "neq" )
        return { true, rhs ? boolean { rhs -> val != b . val } : boolean { true } };
    if ( ! rhs )
        throw std::runtime_error ( "Boolean expected as the operand to "s + _method_name . data () );
    if ( _method_name == "&" || _method_name == "and" )
        return { true, boolean { b . val && rhs -> val } };
    if ( _method_name == "|" || _method_name == "or" )
        return { true, boolean { b . val || rhs -> val } };
    invalid_method_exception ( "integer", _method_name );
}

fml::method_dispatch_result fml::method_dispatcher::operator () ( fml::types::arr_ref ref )
{
    if ( std::size ( _args ) < 1 )
        throw std::runtime_error ( "At least 1 argument expected to the call of "s + _method_name . data () );
    auto arg = _args . front();

    _r -> ip ++;
    auto idx = std::get<integer> ( arg ) . val;
    if ( idx < 0 || static_cast<std::size_t> ( idx ) >= ref . arr -> size  )
        throw std::runtime_error ( "Index "s + std::to_string ( idx ) + " is out of array bounds." );

    if ( _method_name == "get" )
        return { true, ref . arr -> data()[idx]  };
    if ( _method_name == "set" )
    {
        check_arg_cnt ( 2 );
        auto val = _args . back();
        ref . arr -> data()[idx] = val;
        return { true, val };
    }
    invalid_method_exception ( "array", _method_name );
}

fml::method_dispatch_result fml::method_dispatcher::operator () ( fml::types::obj_ref ref )
{
    auto c = std::get<fml::types::class_t> ( _r -> constant_pool[ref . obj -> class_t_index] );
    if ( auto it = std::ranges::find_if ( c . methods, [this] ( const auto & m ) { return m . name == this -> _method_name; }); it != std::end ( c . methods ) )
    {
        check_arg_cnt ( it -> args - 1 ); // 1 for the object itself
        _r -> frames . push_object_frame( ref, _args, it -> locals, _r -> ip + 1 );
        _r -> ip = it -> start;
        return { true, { } };
    }
    return {false, { } };
}
