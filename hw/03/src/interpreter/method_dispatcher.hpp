//
// Created by Jan Groschaft on 3/19/21.
//

#ifndef INC_03_METHOD_DISPATCHER_HPP
#define INC_03_METHOD_DISPATCHER_HPP

#include "runtime/runtime.hpp"
#include "types/types.hpp"

#include <string_view>
#include <optional>

namespace fml
{
    struct method_dispatch_result
    {
        bool dispatch_complete { false };
        std::optional<types::pointer> result;
    };

    class method_dispatcher
    {
        std::string_view _method_name;
        std::span<types::pointer> _args;
        fml::runtime * const _r;

        void check_arg_cnt ( std::size_t expected_cnt );
    public:
        method_dispatcher ( std::string_view method_name, std::span<types::pointer> args, runtime * r );

        method_dispatch_result operator () ( fml::types::integer i );
        method_dispatch_result operator () ( fml::types::null n );
        method_dispatch_result operator () ( fml::types::boolean b );
        method_dispatch_result operator () ( fml::types::arr_ref ref );
        method_dispatch_result operator () ( fml::types::obj_ref ref );
    };
}

#endif //INC_03_METHOD_DISPATCHER_HPP
