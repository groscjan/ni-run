//
// Created by Jan Groschaft on 3/16/21.
//

#include "utils/output.hpp"


using namespace std::literals::string_literals;

void fml::print_to_stdout ( std::string_view format, std::span<types::pointer> args, const std::vector<types::program_object> & const_pool )
{
    for ( auto i = 0ul; i < std::size ( format ); ++i )
    {
        const auto c = format[i];
        if ( c == print_escape_symbol )
        {
            if ( std::empty( args ))
                throw std::runtime_error ( "Too few arguments to print call." );
            std::cout << to_string ( args . front(), const_pool );
            args = args . last ( std::size ( args ) - 1 );
        } else if ( c == '\\' )
        {
            if ( ++i == std::size( format) )
                throw std::runtime_error ( "Symbol expected after '\\'" );
            switch ( format[i] )
            {
                case '~':
                    std::cout << "~";
                    break;
                case 'n':
                    std::cout << '\n';
                    break;
                case '"':
                    std::cout << '\"';
                    break;
                case 'r':
                    std::cout << '\r';
                    break;
                case 't':
                    std::cout << '\t';
                    break;
                case '\\':
                    std::cout << '\\';
                    break;
                default:
                    throw std::runtime_error ( "Invalid symbol after escape: "s + format[i] );
            }
        } else
        {
            std::cout << c;
        }
    }

    if ( ! std::empty(args) )
        throw std::runtime_error ( "Too many arguments to print call" );
}


