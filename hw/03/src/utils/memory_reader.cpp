//
// Created by Jan Groschaft on 3/13/21.
//

#include "utils/memory_reader.hpp"


std::string_view fml::utils::read_string ( const std::byte *& memory, std::size_t size )
{
    auto ret = std::string_view { reinterpret_cast<const char*> (memory), size };
    memory += size;
    return ret;
}
