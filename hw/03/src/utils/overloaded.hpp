//
// Created by Jan Groschaft on 3/16/21.
//

#ifndef INC_03_OVERLOADED_HPP
#define INC_03_OVERLOADED_HPP

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

#endif //INC_03_OVERLOADED_HPP
