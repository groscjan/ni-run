//
// Created by Jan Groschaft on 3/12/21.
//

#ifndef INC_03_BUFFER_HPP
#define INC_03_BUFFER_HPP

#include <memory>

namespace fml::utils
{
    template <typename T>
    struct buffer
    {
        std::size_t len;
        std::unique_ptr<T[]> data;
    };
}

#endif //INC_03_BUFFER_HPP
