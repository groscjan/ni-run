//
// Created by Jan Groschaft on 3/16/21.
//

#ifndef INC_03_OUTPUT_HPP
#define INC_03_OUTPUT_HPP

#include "types/types.hpp"

#include <string_view>
#include <vector>
#include <span>
#include <iostream>


namespace fml
{
    struct runtime;

    static constexpr char print_escape_symbol = '~';


    void print_to_stdout ( std::string_view format, std::span<types::pointer> args, const std::vector<types::program_object> & const_pool );
}
#endif //INC_03_OUTPUT_HPP
