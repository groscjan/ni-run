//
// Created by Jan Groschaft on 3/12/21.
//

#ifndef INC_03_MEMORY_READER_HPP
#define INC_03_MEMORY_READER_HPP

#include <cstdint>
#include <cstddef>
#include <tuple>
#include <concepts>
#include <string_view>
#include <iostream>

// utils for reading data form memory stored as little-endian

namespace fml::utils
{
    template <std::integral type>
    type read_integral ( const std::byte *& memory )
    {
        auto size_bytes = sizeof (type);
        std::integral auto res = type { 0 };
        for ( auto i = 0ul; i < size_bytes; ++i )
            res |= std::to_integer<type>( memory[i] ) << ( 8 * i );
        memory += size_bytes;
        return res;
    }

    std::string_view read_string ( const std::byte *& memory, std::size_t size );
}

#endif //INC_03_MEMORY_READER_HPP
