//
// Created by Jan Groschaft on 3/12/21.
//

#include "serialization/serialization.hpp"
#include "utils/memory_reader.hpp"

#include <fstream>
#include <cassert>
#include <unordered_map>

using namespace std::literals::string_literals;
using namespace fml;
using namespace serialization;
using namespace utils;


buffer<std::byte> serialization::load_bytecode ( std::string_view file_path )
{
    auto ifs = std::ifstream { file_path . data (), std::ios::binary | std::ios::ate };
    if ( ifs . bad() || ! ifs . is_open() )
        throw std::invalid_argument("Error reading bytecode from file "s + file_path.data() );
    auto size = ifs . tellg ();
    ifs . seekg ( 0, std::ios::beg );

    auto ret = buffer<std::byte> { static_cast<size_t>(size), std::make_unique<std::byte[]> ( size ) };
    ifs . read ( reinterpret_cast<char *>( ret . data . get() ), size );

    return ret;
}

template <typename T>
auto get_constant ( const std::vector<types::program_object> & const_pool, const std::size_t idx )
{
    assert(idx < std::size(const_pool) && "Invalid reference to an undeclared constant.");
    return std::get<T> (const_pool[idx]);
}

auto parse_instruction ( const std::byte *& data, std::vector<label_info> & labels, std::size_t instruction_idx )
{
    auto type = instruction_opcode { read_integral<std::uint8_t>( data ) };
    switch ( type )
    {
        case instruction_opcode::literal:
            return types::instruction { types::literal { read_integral<types::index_t>(data) }};
        case instruction_opcode::get_loc:
            return types::instruction { types::get_loc { read_integral<types::index_t>(data) }};
        case instruction_opcode::set_loc:
            return types::instruction { types::set_loc { read_integral<types::index_t>(data) }};
        case instruction_opcode::get_glob:
            return types::instruction { types::get_glob { read_integral<types::index_t>(data) }};
        case instruction_opcode::set_glob:
            return types::instruction { types::set_glob { read_integral<types::index_t>(data) }};
        case instruction_opcode::call_fn:
        {
            std::integral auto index = read_integral<types::index_t> ( data );
            std::integral auto arg_cnt = read_integral<uint8_t> ( data );
            return types::instruction { types::fn_call { index, arg_cnt } };
        }
        case instruction_opcode::ret:
            return types::instruction { types::ret { }};
        case instruction_opcode::label:
        {
            const auto label = types::label { read_integral<types::index_t> ( data ) };
            labels . push_back ( {label, instruction_idx} );
            return types::instruction { label };
        }
        case instruction_opcode::jump:
            return types::instruction { types::jump { read_integral<types::index_t>(data) }};
        case instruction_opcode::branch:
            return types::instruction { types::branch { read_integral<types::index_t>(data) }};
        case instruction_opcode::print:
        {
            std::integral auto index = read_integral<types::index_t> ( data );
            std::integral auto arg_cnt = read_integral<uint8_t> ( data );
            return types::instruction { types::print { index, arg_cnt } };
        }
        case instruction_opcode::arr:
            return types::instruction { types::arr_def { }};
        case instruction_opcode::object:
            return types::instruction { types::obj_def { read_integral<types::index_t>(data) }};
        case instruction_opcode::get_field:
            return types::instruction { types::get_field { read_integral<types::index_t>(data) }};
        case instruction_opcode::set_field:
            return types::instruction { types::set_field { read_integral<types::index_t>(data) }};
        case instruction_opcode::call_m:
        {
            std::integral auto index = read_integral<types::index_t> ( data );
            std::integral auto arg_cnt = read_integral<uint8_t> ( data );
            return types::instruction { types::m_call { index, arg_cnt } };
        }
        case instruction_opcode::drop:
            return types::instruction { types::drop { }};
        default:
            throw std::out_of_range ("Invalid instruction.");
    }
}


// this method assumes that constant c can't reference another constant that is declared after c
auto parse_const_pool ( const std::byte *& data )
{
    std::integral auto const_pool_size = read_integral<uint16_t>( data );

    auto instructions = std::vector<types::instruction> {};
    auto labels = std::vector<label_info> {};
    auto const_pool = std::vector<types::program_object> {};
    const_pool . reserve( const_pool_size );

    for ( auto i = 0u; i < const_pool_size; ++i )
    {
        auto type = program_object_opcode { read_integral<std::uint8_t>( data ) };
        switch ( type )
        {
            case program_object_opcode::integer:
                const_pool . emplace_back( types::integer{ read_integral<int32_t>( data ) } );
                break;
            case program_object_opcode::boolean:
                const_pool . emplace_back( types::boolean { read_integral<bool>( data ) } );
                break;
            case program_object_opcode::null:
                const_pool . emplace_back( types::null {} );
                break;
            case program_object_opcode::string:
            {
                std::integral auto len = read_integral<uint32_t>( data );
                const_pool . emplace_back( types::string { read_string( data, len ) } );
                break;
            }
            case program_object_opcode::slot:
            {
                std::integral auto slot_idx = read_integral<uint16_t> ( data );
                const_pool . emplace_back ( types::slot { get_constant<types::string>( const_pool, slot_idx ) . str } );
                break;
            }
            case program_object_opcode::method:
            {
                std::integral auto name_idx = read_integral<uint16_t> ( data );
                auto name = get_constant<types::string> ( const_pool, name_idx ) . str;
                std::integral auto arg_cnt = read_integral<uint8_t> ( data );
                std::integral auto local_cnt = read_integral<uint16_t> ( data );
                std::integral auto length = read_integral<uint32_t> ( data );
                auto start = static_cast<uint32_t> ( std::size ( instructions ) );

                for ( auto j = 0u; j < length; ++j )
                {
                    auto instr = parse_instruction ( data, labels, std::size ( instructions ) );
                    instructions . push_back( instr );
                }

                const_pool . emplace_back ( types::method { name, start, length, local_cnt, arg_cnt } );
                break;
            }
            case program_object_opcode::class_t:
            {
                std::integral auto member_cnt = read_integral<uint16_t>( data );

                auto class_def = types::class_t { };
                for ( auto j = 0u; j < member_cnt; ++j )
                {
                    std::integral auto idx = read_integral<uint16_t>( data );
                    assert( idx < std::size ( const_pool ) && "Invalid constant index.");

                    if ( std::holds_alternative<types::slot>( const_pool[idx]) )
                        class_def . fields . push_back( std::get<types::slot> ( const_pool[idx] ) );
                    else if ( std::holds_alternative<types::method>( const_pool[idx]))
                        class_def . methods . push_back( std::get<types::method> ( const_pool[idx] ) );
                    else throw std::out_of_range ( "Slot or method expected." );
                }
                const_pool . emplace_back( class_def );
                break;
            }
            default:
                throw std::out_of_range ("Invalid program obj_def.");
        }
    }

    return std::make_tuple( std::move (const_pool), std::move (instructions), std::move ( labels ));
}

auto parse_globals ( const std::byte *& data, const std::vector<types::program_object> & const_pool )
{
    std::integral auto global_cnt = read_integral<uint16_t>( data );

    auto globals = std::vector<std::string_view> {};
    auto functions = std::unordered_map<std::string_view, types::method> {};

    for ( auto i = 0u; i < global_cnt; ++i )
    {
        std::integral auto idx = read_integral<uint16_t>( data );
        assert(idx < std::size ( const_pool ) && "Invalid index into constant pool" );

        if ( std::holds_alternative<types::slot>( const_pool[idx] ) )
            globals . emplace_back( get_constant<types::slot>( const_pool, idx ) . name );
        else if ( std::holds_alternative<types::method>( const_pool[idx]))
        {
            auto method = get_constant<types::method>( const_pool, idx );
            if ( ! functions . insert ( {method.name, method} ) . second )
                throw std::runtime_error ( "Redefinition of function "s + method.name.data() );
        }
        else throw std::out_of_range ( "Globals can only point to slots or methods.");
    }

    return std::make_tuple ( std::move (globals),std::move ( functions) );
}


program serialization::parse_bytecode ( const buffer<std::byte> & bc )
{
    const auto * data = bc . data . get ();

    auto [const_pool, instructions, labels] = parse_const_pool( data );
    auto [globals, functions] = parse_globals( data, const_pool );
    auto main = get_constant<types::method>( const_pool, read_integral<std::uint16_t> ( data ) );

    return { std::move (const_pool), std::move (globals), std::move (functions), main, std::move ( instructions ), std::move ( labels ) };
}
