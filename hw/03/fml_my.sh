#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 run <program.fml>"
    exit 1
fi

base_dir=$(dirname "$0")

fml_exe="$base_dir"/../../FML/target/release/fml
compiler_exe="$base_dir"/../04/build/compiler
interpreter_exe="$base_dir"/build/bc
temp_json=tmp.json
temp_bc=tmp.bc

./"$fml_exe" parse --format JSON < "$2" > "$temp_json" && ./"$compiler_exe" "$temp_json" > "$temp_bc"

if [ "$?" -ne 0 ]; then
    exit 1
fi 

./"$interpreter_exe" "$temp_bc"
rm "$temp_bc"
rm "$temp_json"



