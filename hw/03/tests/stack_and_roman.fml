// Expected output:
// > decimal: 0
// > roman: 
// > decimal: 1
// > roman: I
// > decimal: 2
// > roman: II
// > decimal: 3
// > roman: III
// > decimal: 4
// > roman: IV
// > decimal: 5
// > roman: V
// > decimal: 6
// > roman: VI
// > decimal: 7
// > roman: VII
// > decimal: 8
// > roman: VIII
// > decimal: 9
// > roman: IX
// > decimal: 10
// > roman: X
// > decimal: 40
// > roman: XL
// > decimal: 50
// > roman: L
// > decimal: 90
// > roman: XC
// > decimal: 100
// > roman: C
// > decimal: 400
// > roman: CD
// > decimal: 500
// > roman: D
// > decimal: 900
// > roman: CM
// > decimal: 1000
// > roman: M
// > decimal: 6
// > roman: VI
// > decimal: 42
// > roman: XLII
// > decimal: 666
// > roman: DCLXVI
// > decimal: 14000
// > roman: MMMMMMMMMMMMMM



function stack() -> object
begin
    let val = null;
    let next = null;

    function push(val) ->
    begin
        let node = stack();
        node.val <- this.val;
        node.next <- this.next;
        this.val <- val;
        this.next <- node;
    end;

    function pop() ->
    begin
        let ret = this.val;
        this.val <- this.next.val;
        this.next <- this.next.next;
        ret;
    end;

    function peek() ->
        this.val;
end;

function print_roman_literal (val) ->
begin
    if val == 1 then print("I")
    else if val == 4 then print("IV")
    else if val == 5 then print("V")
    else if val == 9 then print("IX")
    else if val == 10 then print("X")
    else if val == 40 then print("XL")
    else if val == 50 then print("L")
    else if val == 90 then print("XC")
    else if val == 100 then print("C")
    else if val == 400 then print("CD")
    else if val == 500 then print("D")
    else if val == 900 then print("CM")
    else if val == 1000 then print("M");
end;

function create_literals() ->
begin
    let s = stack();
    s.push( 1 );
    s.push( 4 );
    s.push( 5 );
    s.push( 9 );
    s.push( 10 );
    s.push( 40 );
    s.push( 50 );
    s.push( 90 );
    s.push( 100 );
    s.push( 400 );
    s.push( 500 );
    s.push( 900 );
    s.push( 1000 );
    s
end;


function to_roman_rec (val, literals) ->
begin
    if val > 0 then
    begin
        let current_literal = literals.peek();
        if val >= current_literal then
        begin
            print_roman_literal (current_literal);
            val <- val - current_literal;
        end
        else literals.pop();
    
        to_roman_rec(val, literals);    
    end;
end;


function int_ext (i) -> object extends i
begin
    let val = i;
    function print_decimal () -> print("~\n", this.val);
    
    function print_roman () -> 
    begin
        let literals = create_literals();
        to_roman_rec(this.val, literals);
        print("\n"); 
    end;
end;


function test(i) -> 
begin
    let num = int_ext(i);
    print("decimal: ");
    num.print_decimal();
    print("roman: ");
    num.print_roman();
end;

let i = 0;
while i <= 10 do
begin 
    test(i);
    i <- i + 1;
end;

test(40);
test(50);
test(90);
test(100);
test(400);
test(500);
test(900);
test(1000);
test(6);
test(42);
test(666);
test(14000);
